#include <cmath>
#include <iostream>

#include "package_structures.h"

using namespace std;

// Position
Position::Position(): x(0), y(0) {}
Position::Position(double x, double y): x(x), y(y) {}
Position::Position(const Position &p): x(p.x), y(p.y) {}
bool Position::operator == (const Position &right) const
{
    return (fabs(x - right.x) < POSITION_EPS) &&
           (fabs(y - right.y) < POSITION_EPS);
}


// Sprite
Sprite::Sprite(): ID(0), action(0), frame(0), frame_cnt(0) {}
Sprite::Sprite(int sId): ID(sId), action(0), frame(0), frame_cnt(0) {}
Sprite::Sprite(int sId, int sAction, int sFrame, int sFrameCnt): ID(sId), action(sAction),
                                                         frame(sFrame), frame_cnt(sFrameCnt) {}
Sprite::Sprite(const Sprite &s): ID(s.ID), action(s.action),
                                   frame(s.frame), frame_cnt(s.frame_cnt) {}
bool Sprite::operator == (const Sprite &right) const
{
    return tie(ID, action, frame, frame_cnt) ==
           tie(right.ID, right.action, right.frame, right.frame_cnt);
}
