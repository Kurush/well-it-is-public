//
// Created by znujko on 24.11.2019.
//

#ifndef MAIN_PROJECT_GRAPHIC_H
#define MAIN_PROJECT_GRAPHIC_H

#include "data.h"

class GameSprite;
class GameWindow;


/** Р­С‚РѕС‚ РєР»Р°СЃСЃ С‚РµР±Рµ РЅСѓР¶РµРЅ, С‡С‚РѕР±С‹ РІР·Р°РёРјРѕРґРµР№СЃС‚РІРѕРІР°С‚СЊ СЃРѕ РјРЅРѕР№,
 * РўС‹ РїСЂРѕСЃС‚Рѕ РІС‹Р·С‹РІР°РµС€СЊ РµРіРѕ РјРµС‚РѕРґС‹ РґР»СЏС‚РѕРіРѕ, С‡С‚РѕР±С‹ СЃРґРµР»Р°С‚СЊ С‚Рµ РёР»Рё РёРЅС‹Рµ РґРµР№СЃС‚РІРёСЏ
 *
 * createSprite(int spriteID)                                                РІРµСЂРЅРµС‚ С‚РµР±Рµ СѓРєР°Р·Р°С‚РµР»СЊ РЅР° GameSprite, РєРѕС‚РѕСЂС‹Р№
 *                                                                           С‚РµР±Рµ РЅР°РґРѕ СЃРѕС…СЂР°РЅРёС‚СЊ РІ СЃРІРѕСЋ РјР°РїСѓ
 *
 * updateSprite(GameSprite* spriteGr, Sprite sprite, Position position)      РѕР±РЅРѕРІРёС‚ СЃРїСЂР°Р№С‚ РїРѕ Р·Р°РґР°РЅРЅС‹Рј С‚РѕР±РѕР№ РїР°СЂР°РјРµС‚СЂР°Рј
 *
 * deleteSprite(GameSprite* spriteGr)                                        СѓРґР°Р»РёС‚ СЃР°Рј РѕР±СЉРµРєС‚ СЃРїСЂР°Р№С‚Р° Сѓ РјРµРЅСЏ**/


class SpriteGraphic
{
private:
    GameWindow* parent_;


public:


    GameSprite* createSprite(int spriteID);
    void updateSprite(GameSprite* spriteGr, Sprite sprite, Position position);
    void deleteSprite(GameSprite* spriteGr);

    SpriteGraphic(SpriteGraphic *pGraphic);

    SpriteGraphic();
};



/**  Р­С‚Рѕ СЃРѕР±СЃС‚РІРµРЅРЅРѕ СЃР°Рј РєР»Р°СЃСЃ GameSprite, РєРѕС‚РѕСЂС‹Р№ Рё С…СЂР°РЅРёРёС‚ РІ СЃРµР±Рµ РІСЃРµ РґР°РЅРЅС‹Рµ Рѕ РЅСѓР¶РЅРѕР№ РµРјСѓ РіСЂР°С„РёРєРµ **/




#endif //MAIN_PROJECT_GRAPHIC_H
