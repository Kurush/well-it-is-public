cmake_minimum_required(VERSION 3.10.2)

project(GameSprite)

add_library(${PROJECT_NAME} src/gameSprite.cpp)
add_library(sub::gameSprite ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers
        )

