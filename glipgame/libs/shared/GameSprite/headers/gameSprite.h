//
// Created by znujko on 24.11.2019.
//

#ifndef MAIN_PROJECT_GAMESPRITE_H
#define MAIN_PROJECT_GAMESPRITE_H
class GameSprite{
private:

    int spriteID_;

public:
    explicit GameSprite(int spriteID);
    void setSprite(int action, int frame);
    void setPosition(float x, float y);
    ~GameSprite();


};
#endif //MAIN_PROJECT_GAMESPRITE_H
