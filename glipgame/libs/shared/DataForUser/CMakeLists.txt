cmake_minimum_required(VERSION 3.10.2)

project(gameDataStorage)

add_library(${PROJECT_NAME} src/gameDataStorage.cpp)
add_library(sub::dataStorage ALIAS ${PROJECT_NAME})


target_include_directories(${PROJECT_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/headers

)

target_link_libraries(${PROJECT_NAME}
        sub::package
        sub::UITypes
        sub::data
        )