//
// Created by znujko on 07.11.2019.
//

#include <memory>
#include "gameDataStorage.h"




Commands ActionQueue::popAction() {

    mute.lock();

    if (actionQueue.empty()) {
        return Commands::ZERO;
    }

    Commands result = actionQueue.front();
    actionQueue.pop();
    mute.unlock();
    return result;

}

void ActionQueue::addAction(Commands stringAdded) {
    mute.lock();
    actionQueue.push(stringAdded);
    mute.unlock();

}

ActionQueue::ActionQueue() {

    while (!actionQueue.empty())
        actionQueue.pop();

}

ActionQueue::~ActionQueue() {

    while (!actionQueue.empty())
        actionQueue.pop();
}


SpriteQueue::SpriteQueue() {

    while (!spriteQueue.empty())
        spriteQueue.pop();

}

SpriteQueue::~SpriteQueue() {

    while (!spriteQueue.empty())
        spriteQueue.pop();

}


std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>> SpriteQueue::popAction() {

    mutexSPRITE.lock();

    std::pair<std::pair<ActionsUI, GameSprite *>, std::pair<Sprite, Position>> ret = spriteQueue.front();
    spriteQueue.pop();

    mutexGSPRITE.unlock();
    return ret;


}

bool SpriteQueue::spriteQueueisEmpty() {
    return spriteQueue.empty();
}


void   SpriteQueue::addAction(ActionsUI actionType, GameSprite *&gSprite, const Sprite &sprite, const Position &position) {

    mutexSPRITE.lock();
    auto p1 = std::make_pair(actionType, gSprite);
    auto p2 = std::make_pair(sprite, position);
    auto p = std::make_pair(p1, p2);


    spriteQueue.push(p);


    mutexSPRITE.unlock();

}

GameSprite *SpriteQueue::popGameSprite() {

    mutexGSPRITE.lock();

    if (gameSpriteQueue.empty())
    {
        mutexGSPRITE.unlock();
        return nullptr;
    }

    auto ret = gameSpriteQueue.front();
    gameSpriteQueue.pop();

    mutexGSPRITE.unlock();

    return ret;

}

void SpriteQueue::addGameSprite(GameSprite *&gSprite) {

    mutexGSPRITE.lock();

    gameSpriteQueue.push(gSprite);

    mutexGSPRITE.unlock();

}
