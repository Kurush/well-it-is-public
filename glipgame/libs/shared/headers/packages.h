#ifndef PACKAGES_H
#define PACKAGES_H

#include <vector>
#include <string>
#include "package_structures.h"

#include "connector.h"

/// TODO: include some server header. remove this thing
/**
    \brief  Интерфейс сообщений, которые будут отправляться в EventLoop

    Для отправки сообщений, необходимо реализовать данный интерфейс и метод GetType должен
    возвращать тип события. По нему будет определяться какой обработчик вызывать для этого сообщения

*/

namespace client_packages {
enum Commands
{
    ZERO , LEFTBUTTON, RIGHTBUTTON , UPBUTTON , PICKUP , THROWDOWN , SHOOT ,
    LEFTERASE , RIGHTERASE
};
class CLIENT_PACKAGE : public IMessage {
public :
    CLIENT_PACKAGE(Commands type, double commandTime, unsigned char _userId) : IMessage("SM") {

        typeOfMessage = type;
        messageTime = commandTime;
        userId = _userId;
    }

    Commands typeOfMessage;
    double messageTime;
    unsigned char userId;
};
}

namespace server_packages {
/// TODO: that's client part, must be included from separate header

typedef float PackageGameTime;
typedef enum
{
    DELETE, UPDATE
} ServerPackagePartType;
typedef enum
{
    BULLET, WEAPON, PLAYER
} ObjectType;


/// 1st package type: initial; is sent once - when the player enters the game
/// this package is directly sent to user
struct ServerInitialPackage: public IMessage
{
    /// the ID of a client of server (for server to define where to send package).
    /// Game gets clientID when creating new player, and sends it in every package (both update and init)
    char playerID, serverID, mapID;
    /// game time at which the player enters the game (may not be 0)
    PackageGameTime time;

    ServerInitialPackage(char playerID, char serverID,
                         char mapID, PackageGameTime time): IMessage("Init"), playerID(playerID),
                                                            serverID(serverID), mapID(mapID), time(time) {}

    virtual ~ServerInitialPackage() = default;
};

struct ServerPackagePart;
/// 2nd package type: update; consists of several parts
/// this package is directly sent to user
struct ServerUpdatePackage: public IMessage
{
    /// the ID, which defines Main Hero (for finding it in common vector of update objects)
    /// client gets this ID when connecting t oa game (in ServerInitialPackage) and should
    /// send it in every package
    char playerID;
    /// the ID of a client of server (for server to define where to send package).
    /// Game gets clientID when creating new player, and sends it in every package (both update and init)
    char serverID;
    PackageGameTime time;
    /// main hero is under 'playerID' ID and type of 'Player'
    std::vector<ServerPackagePart*> data;

    ServerUpdatePackage(char playerID, char serverID,
                        PackageGameTime time): IMessage("Update"), playerID(playerID),
                                               serverID(serverID), time(time)
    {
        data.clear();
    }

    virtual ~ServerUpdatePackage()
    {
        data.clear();
    }
};


/// a part of big update package (a 'vector' member)
struct ServerPackagePart
{
    ServerPackagePartType packageType;
    ServerPackagePart(ServerPackagePartType type): packageType(type) {}
};

/// delete package
struct ServerPPDelete: ServerPackagePart
{
    ObjectType objectType;
    int objectID;

    ServerPPDelete(ObjectType t, int id): ServerPackagePart(DELETE), objectType(t), objectID(id) {}
};

/// update package. Each object update pack inherits the one
struct ServerPPUpdate: ServerPackagePart
{
    /// define which: Bullet, Weapon or Player
    ObjectType objectType;
    /// storage ID
    int objectID;
    Sprite sprite;
    Position pos;
    float Vx, Vy; /// speeds of object

    ServerPPUpdate(ObjectType t, int id,
                   Sprite sprite, Position pos, float vx, float vy): ServerPackagePart(UPDATE), objectType(t), objectID(id),
                                                       sprite(sprite), pos(pos), Vx(vx), Vy(vy){}
};

/// update for object of Bullet
struct ServerPPUpdateBullet: ServerPPUpdate
{
    short damage;

    ServerPPUpdateBullet(int bulletID, Sprite sprite, Position pos, float vx, float vy,
                         short damage): ServerPPUpdate(BULLET, bulletID, sprite, pos, vx, vy),
                                                    damage(damage) {}
};
/// update for object of Weapon
struct ServerPPUpdateWeapon: ServerPPUpdate
{
    short capacity;
    bool isPicked;

    ServerPPUpdateWeapon(int weaponID, Sprite sprite, Position pos, float vx, float vy,
                         short capacity, bool picked): ServerPPUpdate(WEAPON, weaponID, sprite, pos, vx, vy),
                                                                     capacity(capacity), isPicked(picked) {}
};
struct ServerPPUpdatePlayer: ServerPPUpdate
{
    short HP;
    /// real ID if weapon exists; negative value otherwise
    int weaponID;

    ServerPPUpdatePlayer(int playerID, Sprite sprite, Position pos, float vx, float vy,
                         short HP, int weaponID = -1): ServerPPUpdate(PLAYER, playerID, sprite, pos, vx, vy),
                                                              HP(HP), weaponID(weaponID) {}
};
}

#endif // PACKAGES_H
