#ifndef PACKAGE_STRUCTURES_H
#define PACKAGE_STRUCTURES_H

///\authors Kazakov Kirill

#include <iostream>
#include <string>
#include <set>
#include <tuple>

const double POSITION_EPS = 1e-5;

/// \brief Game coordinates (do not mix with 'Coordinates' struct)
/**
 \details Game coordinate system may differ from carthesian (which is in physics).
          When needed to translate one to another, use CoordinateTranslator.
          This struct implements a "matrix coordinates": left-up corner has (0,0) coordinate,
          Y-axis is down, X-axis is right
 */
struct Position
{
    double x, y;
    Position();
    Position(double x, double y);
    Position(const Position &p);
    bool operator == (const Position &right) const;
};

/**
\brief Sprite identification
\details
- ID defines, which sprite sheet to use
- action defines a row in sprite sheet (chooses the action like 'run left', 'jump' etc.)
- frame_cnt defines the number of frames in each action
- Sprites should be updated using SpriteUpdater
*/
struct Sprite
{
    int ID;
    int action;
    int frame;
    int frame_cnt;

    Sprite();
    Sprite(int sId);
    Sprite(int sId, int sAction, int sFrame, int sFrameCnt);
    Sprite(const Sprite &s);
    bool operator == (const Sprite &right) const;
};

#endif // PACKAGE_STRUCTURES_H
