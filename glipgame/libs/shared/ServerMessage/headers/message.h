//
// Created by znujko on 24.11.2019.
//

#ifndef MAIN_PROJECT_MESSAGE_H
#define MAIN_PROJECT_MESSAGE_H


#include <string>
#include <memory>

#include "connector.h"

using IEventHandler = std::shared_ptr<IMessage> (std::shared_ptr<IMessage> message);

#endif //MAIN_PROJECT_MESSAGE_H
