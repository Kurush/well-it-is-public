#include "server_connections.h"
#include "game_objects/game_storage.h"
#include "server_game.h"

#include <memory>
#include <iostream>
#include <functional>

using namespace std;

// client package handler
ClientPackageHandler::ClientPackageHandler(LogicUpdater *upd): updater(upd)
{
    command_interpret.clear();
    command_interpret[client_packages::LEFTBUTTON] = LEFT_START;
    command_interpret[client_packages::LEFTERASE] = LEFT_END;
    command_interpret[client_packages::RIGHTBUTTON] = RIGHT_START;
    command_interpret[client_packages::RIGHTERASE] = RIGHT_END;
    command_interpret[client_packages::UPBUTTON] = JUMP;
    command_interpret[client_packages::PICKUP] = PICKWEAPON;
    command_interpret[client_packages::THROWDOWN] = THROWWEAPON;
    command_interpret[client_packages::SHOOT] = SSHOOT;

    supportedActionTypes.clear();
    supportedActionTypes.insert(client_packages::LEFTBUTTON);
    supportedActionTypes.insert(client_packages::LEFTERASE);
    supportedActionTypes.insert(client_packages::RIGHTBUTTON);
    supportedActionTypes.insert(client_packages::RIGHTERASE);
    supportedActionTypes.insert(client_packages::UPBUTTON);
    supportedActionTypes.insert(client_packages::PICKUP);
    supportedActionTypes.insert(client_packages::THROWDOWN);
    supportedActionTypes.insert(client_packages::SHOOT);
}

bool ClientPackageHandler::isSupported(client_packages::Commands type)
{
    return supportedActionTypes.count(type) != 0;
}

std::shared_ptr<IMessage> ClientPackageHandler::eventHandler(std::shared_ptr<IMessage> message)
{
    shared_ptr<client_packages::CLIENT_PACKAGE>  pack =
            reinterpret_pointer_cast<client_packages::CLIENT_PACKAGE>(message);

    if (isSupported(pack->typeOfMessage))
    {
        this->updater->updatePlayerAction(pack->userId, command_interpret[pack->typeOfMessage]);
    }
    return message;
}

// players handler

PlayersHandler::PlayersHandler(ServerGame *game): game(game) {}
std::shared_ptr<IMessage> PlayersHandler::addPlayerHandler(std::shared_ptr<IMessage> message)
{
    //TODO cast & parse
    int id = 0;
    game->addPlayer(id);
    return message;
}



std::shared_ptr<IMessage> PlayersHandler::removePlayerHandler(std::shared_ptr<IMessage> message)
{
    //TODO cast & parse
    int id = 0;
    game->removePlayer(id);
    return message;
}


// package sender
PackageSender::PackageSender(IConnector *connector, GameStorage *storage,
                             IDTransformer *transformer): connector(connector),
                                                          storage(storage),
                                                          transformer(transformer)   {}
void PackageSender::sendInitPackage(int serverID, int playerID, int mapID, GameTime gameTime)
{

    shared_ptr<server_packages::ServerInitialPackage> p = make_shared<server_packages::ServerInitialPackage>(playerID, serverID, (char)mapID, gameTime);
    connector->sendMessage(p);
}
void PackageSender::sendUpdatePackages(GameTime gameTime)
{
    shared_ptr<server_packages::ServerUpdatePackage> package = make_shared<server_packages::ServerUpdatePackage>(-1, -1, gameTime);

    GameStorageIterator iter;

    // add players
    iter.setIterator(storage, SPLAYER);
    while (!iter.isDone())
    {
        const SPlayer* p = (const SPlayer*)storage->get(SPLAYER, iter.currentID());
        server_packages::ServerPPUpdatePlayer *up_pack = new server_packages::ServerPPUpdatePlayer(iter.currentID(), p->getSprite(),
                                                                                                   p->getPosition(), p->getVelocity().xVel,
                                                                                                   p->getVelocity().yVel, p->getHP(),
                                                                                                   p->getWeaponID());
        package->data.push_back((server_packages::ServerPackagePart*)up_pack);
        iter.next();
    }

    // add weapons
    iter.setIterator(storage, SWEAPON);
    while (!iter.isDone())
    {
        const SWeapon* w = (const SWeapon*)storage->get(SWEAPON, iter.currentID());
        server_packages::ServerPPUpdateWeapon *up_pack = new server_packages::ServerPPUpdateWeapon(iter.currentID(), w->getSprite(), w->getPosition(),
                                                                                                   w->getVelocity().xVel, w->getVelocity().yVel,
                                                                                                   w->getCapacity(), w->getPiked());
        package->data.push_back((server_packages::ServerPackagePart*)up_pack);
        iter.next();
    }

    // add bullets
    iter.setIterator(storage, SBULLET);
    while (!iter.isDone())
    {
        const SBullet* b = (const SBullet*)storage->get(SBULLET, iter.currentID());
        server_packages::ServerPPUpdateBullet *up_pack = new server_packages::ServerPPUpdateBullet(iter.currentID(), b->getSprite(), b->getPosition(),
                                                                                                   b->getVelocity().xVel, b->getVelocity().yVel,
                                                                                                   b->getDamage());
        package->data.push_back((server_packages::ServerPackagePart*)up_pack);
        iter.next();
    }

    auto p = std::reinterpret_pointer_cast<IMessage>(package);
    connector->sendMessage(p);
}


PackageManagers::PackageManagers(IConnector *connector, GameStorage *storage, IDTransformer *transformer,
                                 LogicUpdater *upd, ServerGame *game)
{
    inputHandler = new ClientPackageHandler(upd);
    outputManager = new PackageSender(connector, storage, transformer);
    playerHandler = new PlayersHandler(game);

    outputManager->connector->registerEvent(std::bind(&ClientPackageHandler::eventHandler,
                                                     inputHandler, std::placeholders::_1), "SM");

    // TODO: delete
    outputManager->connector->registerEvent(std::bind(&PlayersHandler::addPlayerHandler,
                                                     playerHandler, std::placeholders::_1), "aaa");
    outputManager->connector->registerEvent(std::bind(&PlayersHandler::removePlayerHandler,
                                                     playerHandler, std::placeholders::_1), "aaa");
}
PackageManagers::~PackageManagers()
{
    delete inputHandler;
    delete outputManager;
    delete playerHandler;
}
