#include "server_game.h"

using namespace std;

// Server Game

struct ServerGameData
{
    PhysicWorld *physWorld;
    PackageManagers *packageManagers;
    GameTime gameTime;
    GameMap *gameMap;
    GameStorage *gameStorage;
    LogicUpdater *logicUpdater;
    IDTransformer idTransformer;
    ServerGameLogger *logger;
    IConnector *connector;

    ~ServerGameData()
    {
        delete gameMap;
        delete logicUpdater;
        delete packageManagers;
        delete gameStorage; // the order of deletes is important! storage must be deleted before PhysicWorld
        delete physWorld;
        delete logger;
        delete connector;
    }
};

ServerGame::ServerGame(IConnector *connector)
{
    data = new ServerGameData();
    data->connector = connector;

    data->gameTime = 0.;
    data->logger = new ServerGameLogger();
    // set random generators
    std::mt19937 gen(time(0));
    std::uniform_int_distribution<> map_id_gen(0, MAP_CNT);
    std::uniform_int_distribution<> weapons_cnt_gen(0, MAX_WEPONS_ON_MAP - MIN_WEPONS_ON_MAP);

    // create map
    int mapID = map_id_gen(gen); //rand() % MAP_CNT;
    data->gameMap = new GameMap(mapID, data->logger);

    // init physic world with gravity and map
    // negative gravity: to accelerate down
    data->physWorld = new PhysicWorld(-10.);
    data->physWorld->addWorldMap(data->gameMap->getPhysMap());
    data->physWorld->setCollisionManager(new CollisionManager(data->physWorld));

    data->gameStorage = new GameStorage();

    // fill game with weapons
    int weapons_amount = weapons_cnt_gen(gen) + MIN_WEPONS_ON_MAP;
    for (int i = 0; i < weapons_amount; ++i)
    {
        addWeapon();
    }

    data->logicUpdater = new LogicUpdater(data->gameStorage, data->physWorld, &data->idTransformer);

    data->packageManagers = new PackageManagers(connector, data->gameStorage,
                                                &data->idTransformer, data->logicUpdater, this);

    // update is needed to resolve collisions of randomly spawned objects with map
    update(0.);
}
ServerGame::~ServerGame()
{
    delete data;
}

void ServerGame::addPlayer(int serverID)
{
    int playerID = rand() % PLAYER_CNT;
    CoordinateTranslator translator(data->gameMap);
    Position spawn_pos = translator.coordinatesToPosition(data->gameMap->getPlayerSpawnCoord());
    SPlayer *new_player = new SPlayer(playerID, spawn_pos, data->physWorld, data->gameMap,
                                    data->gameTime, data->logger);

    int storageID = data->gameStorage->add(SPLAYER, new_player);
    data->idTransformer.addPlayer(serverID, storageID);

    // send initial package back to user
    data->packageManagers->outputManager->sendUpdatePackages(data->gameTime);
    data->packageManagers->outputManager->sendInitPackage(serverID, playerID, data->gameMap->getMapID(), data->gameTime);
}
void ServerGame::removePlayer(int serverID)
{
    int storageID = data->idTransformer.getPlayerStorage(serverID);
    data->gameStorage->remove(SPLAYER, storageID);
}

void ServerGame::addWeapon(int id)
{
    std::mt19937 gen(time(0));
    std::uniform_int_distribution<> weapon_id_gen(0, WEAPONS_CNT);

    int weapon_id = (id >= 0? id: weapon_id_gen(gen));
    CoordinateTranslator translator(data->gameMap);
    Position spawn_pos = translator.coordinatesToPosition(data->gameMap->getWeaponSpawnCoord());
    SWeapon *weapon = new SWeapon(weapon_id, spawn_pos, data->physWorld, data->gameMap,
                                data->gameTime, data->logger);
    int storageID = data->gameStorage->add(SWEAPON, weapon);
}


void ServerGame::updateBullets(GameTime dt, queue<pair<objectType, int>> &toDelete)
{
    GameStorageIterator iter;
    bool ifDestruct;
    iter.setIterator(data->gameStorage, SBULLET);
    while (!iter.isDone())
    {
        ifDestruct = data->logicUpdater->updateBullet(iter.currentID(), data->gameTime);
        if (ifDestruct)
            toDelete.push(make_pair(SBULLET, iter.currentID()));
        iter.next();
    }
}
void ServerGame::updateWeapons(GameTime dt, queue<pair<objectType, int>> &toDelete)
{
    GameStorageIterator iter;
    bool ifDestruct;
    iter.setIterator(data->gameStorage, SWEAPON);
    while (!iter.isDone())
    {
        ifDestruct = data->logicUpdater->updateWeapon(iter.currentID(), data->gameTime);
        if (ifDestruct)
            toDelete.push(make_pair(SWEAPON, iter.currentID()));
        iter.next();
    }
}
void ServerGame::updatePlayers(GameTime dt, queue<pair<objectType, int>> &toDelete)
{
    GameStorageIterator iter;
    bool ifDestruct;
    iter.setIterator(data->gameStorage, SPLAYER);
    while (!iter.isDone())
    {
        ifDestruct = data->logicUpdater->updatePlayer(iter.currentID(), data->gameTime);
        if (ifDestruct)
            toDelete.push(make_pair(SPLAYER, iter.currentID()));
        iter.next();
    }
}
void ServerGame::update(GameTime dt0)
{
    GameTime dt = TIME_UPDATE_STEP;
    // multiple updating
    for (int i = 0; TIME_UPDATE_STEP * i < dt0; ++i) {
    data->gameTime += dt;

    queue<pair<objectType, int>> toDelete;

    GameStorageIterator iter;
    bool ifDestruct;

    // update all objects, order is important!
    updateBullets(dt, toDelete);
    updateWeapons(dt, toDelete);
    updatePlayers(dt, toDelete);

    // erase destructed objects from storage
    pair<objectType, int> p;
    while (!toDelete.empty())
    {
        p = toDelete.front();
        toDelete.pop();
        data->gameStorage->remove(p.first, p.second);
    }

    // update physics. storage is automatically updated
    data->physWorld->UpdateCollisions();
    }
    // send update packages for every user
    data->packageManagers->outputManager->sendUpdatePackages(data->gameTime);
}
