#include <cmath>
#include <iostream>

#include "game_objects/game_objects.h"

using namespace std;
using namespace game_managers;

// Game Object
SGameObject::SGameObject(PhysicWorld *world, GameMap *map, GameTime time): CoordinateTranslator(map), physicBody(nullptr), world(world),
                                                                         physicID(-1), objectTime(time), logger(nullptr) {}
SGameObject::SGameObject(DynamicObject *pBody, Sprite s, PhysicWorld *w, GameMap *map, GameTime time): CoordinateTranslator(map), world(w),
                                                                                                     objectTime(time), logger(nullptr)
{
    sprite = s;
    setPhysicBody(pBody);
}
SGameObject::SGameObject(DynamicObject *pBody, PhysicWorld *w, GameMap *map, GameTime time): CoordinateTranslator(map),
                                                                                           world(w), objectTime(time), logger(nullptr)
{
    setPhysicBody(pBody);
}
SGameObject::SGameObject(const SGameObject &obj)
{
    sprite = obj.sprite;
    world = obj.world;
    setPhysicBody(new DynamicObject(*obj.physicBody));
    objectTime = obj.objectTime;
    setMap(obj.getMap());
    logger = logger;
}
SGameObject::~SGameObject()
{
    erasePhysicBody();
}

void SGameObject::setPosition(Position p)
{
    physicBody->setCoordinates(positionToCoordinates(p));
}
void SGameObject::setPhysicBody(DynamicObject *pBody)
{
    physicBody = pBody;
    physicID = world->addDynamicObject(physicBody);
}

Position SGameObject::getPosition() const
{
    Rectangle rect = (physicBody? physicBody->getRectangle(): Rectangle());
    // position of center. needed - position of left right angle
    rect.coords.x -= rect.width;
    rect.coords.y += rect.height;
    return coordinatesToPosition(rect.coords);
}
Sprite SGameObject::getSprite() const
{
    return sprite;
}
GameTime SGameObject::getTime() const
{
    return objectTime;
}
DynamicObject* SGameObject::getPhysicBody()
{
    return physicBody;
}
DigitalVelocity SGameObject::getVelocity() const
{
    return (physicBody==nullptr ? DigitalVelocity() : physicBody->velocity);
}

void SGameObject::updateTime(GameTime dt)
{
    objectTime += dt;
    if (physicBody != nullptr)
    {
        physicBody->updateTime(dt, world->getGravity());
        // if object is moving - update it's sprite frame
        if ((physicBody->velocity.xVel != 0) || (physicBody->velocity.yVel != 0))
            updateSpriteTime(sprite, objectTime);
    }
}
void SGameObject::erasePhysicBody()
{
    if (physicID >= 0)
        world->removeDynamicObject(physicID);
    delete physicBody;
    physicBody = nullptr;
    physicID = -1;
}

bool SGameObject::operator == (const SGameObject &right) const
{
    return tie(sprite, world, *physicBody) ==
           tie(right.sprite, right.world, *right.physicBody);
}


// SBullet
SBullet::SBullet(int bulletID, Position pos, PhysicWorld *world, GameMap *m, GameTime time, ServerGameLogger *log,
               BulletLoader *loader): SGameObject(world, m, time), loader(loader)
{
    logger = log;
    std::vector<int> sizes;
    double vel_value;
    int frame_cnt;
    int spriteID;
    if (loader->loadBullet(bulletID, spriteID, frame_cnt, vel_value, damage, sizes))
    {
        if (logger != nullptr)
            logger->report_error("<Error: failed to create bullet, default will be set>\n");
        frame_cnt = 1;
        bulletID = -1;
        spriteID = 0;
        sizes.resize(2);
        sizes[0] = sizes[1] = 1;
        vel_value = 30;
        damage = 10;
    }
    sprite.frame_cnt = frame_cnt;
    sprite.action = sprite.frame = 0;
    sprite.ID =spriteID;

    // pos -left-up corner of object. physics need center coordinate!
    Coordinates coords = positionToCoordinates(pos);
    coords.x += sizes[0] / 2;
    coords.y -= sizes[1] / 2;
    Rectangle rect(coords, sizes[0] / 2, sizes[1] / 2);
    DigitalVelocity vel(vel_value, 0);
    DynamicObject *obj = new DynamicObject(rect, 0, vel);

    setPhysicBody(obj);
}
SBullet::~SBullet()
{
    delete loader;
}

int SBullet::getDamage() const
{
    return damage;
}

void SBullet::setDirection(lookDir dir)
{
    if (dir == DIR_LEFT)
        physicBody->velocity.xVel = -physicBody->velocity.xVelValue;
    else
        physicBody->velocity.xVel = physicBody->velocity.xVelValue;
}


// SWeapon
SWeapon::SWeapon(int weaponID, Position pos, PhysicWorld *world, GameMap *m, GameTime time, ServerGameLogger *log,
               WeaponLoader *loader): SGameObject(world, m, time), loader(loader)
{
    logger = log;
    int frame_cnt;
    int spriteID;
    std::vector<int> sizes;
    if (loader->loadWeapon(weaponID, spriteID, frame_cnt, bulletID, bulletCapacity, fireSpeed, sizes))
    {
        if (logger != nullptr)
            logger->report_error("Error: failed to create weapon, default will be set>\n");
        frame_cnt = 1;
        spriteID = 0;
        sizes.resize(2);
        sizes[0] = sizes[1] = 2;
        weaponID = -1;
        bulletID = 0;
        bulletCapacity = 1;
        fireSpeed = 1;
    }
    sprite.frame_cnt = frame_cnt;
    sprite.action = sprite.frame = 0;
    sprite.ID =spriteID;

    isPicked = false;
    timeFromShot = 0.;

    // pos -left-up corner of object. physics need center coordinate!
    Coordinates coords = positionToCoordinates(pos);
    coords.x += sizes[0] / 2;
    coords.y -= sizes[1] / 2;
    // semiwidths
    Rectangle rect(coords, sizes[0] / 2, sizes[1] / 2);
    DigitalVelocity vel(0, 0);
    DynamicObject *obj = new DynamicObject(rect, 1, vel);

    setPhysicBody(obj);
}
SWeapon::~SWeapon()
{
    delete loader;
}
bool SWeapon::getPiked() const
{
    return isPicked;
}
int SWeapon::getCapacity() const
{
    return bulletCapacity;
}
int SWeapon::getBulletID() const
{
    return bulletID;
}
double SWeapon::getFireSpeed() const
{
    return fireSpeed;
}
GameTime SWeapon::getTimeFromShot()
{
    return timeFromShot;
}

void SWeapon::onPickUp()
{
    DynamicObject *obj_copy = new DynamicObject(*physicBody);
    erasePhysicBody();
    // physic body remains, but's not contained in physic world
    physicBody = obj_copy;
    isPicked = true;
}
void SWeapon::onThrow(Position pos)
{
    // pos -left-up corner of object. physics need center coordinate!
    Coordinates coords = positionToCoordinates(pos);
    coords.x += this->getPhysicBody()->getRectangle().width;
    coords.y -= this->getPhysicBody()->getRectangle().height;
    physicBody->setCoordinates(coords);
    physicBody->velocity.xVel = physicBody->velocity.yVel = 0;
    setPhysicBody(physicBody);
    isPicked = false;

}
SBullet* SWeapon::shoot(lookDir look, double shift, Position pos)
{
    if (timeFromShot < fireSpeed)
        return nullptr;

    // change pos in order not to hit the archer himself
    if (look == DIR_RIGHT)
        pos.x += shift;
    else
        pos.x -= shift;

    SBullet* bullet = new SBullet(bulletID, pos, world,
                                getMap(), objectTime, logger);
    this->timeFromShot = 0;
    this->bulletCapacity--;
    bullet->setDirection(look);
    return bullet;
}
void SWeapon::updateTime(GameTime dt)
{
    SGameObject::updateTime(dt);
    timeFromShot += dt;
}


// SPlayer
SPlayer::SPlayer(int playerID, Position pos, PhysicWorld *world, GameMap* m, GameTime time, ServerGameLogger *log,
       PlayerLoader *loader): SGameObject(world, m, time), loader(loader)
{
    logger = log;
    int frame_cnt;
    int spriteID;
    std::vector<int> sizes;
    double speedX, speedY;
    if (loader->loadPlayer(playerID, spriteID, frame_cnt, maxHP, speedX, speedY, sizes))
    {
        if (logger != nullptr)
            logger->report_error("<Error: failed to create player, default will be set>\n");
        frame_cnt = 1;
        playerID = -1;
        spriteID = 0;
        sizes.resize(2);
        sizes[0] = sizes[1] = 2;

        maxHP = 1000;
        speedX = speedY = 5;
    }
    sprite.frame_cnt = frame_cnt;
    sprite.action = sprite.frame = 0;
    sprite.ID =spriteID;

    HP = maxHP;
    weaponID = -1;
    lookDirection = DIR_RIGHT; // because action = 0, which stands for move right
    // pos -left-up corner of object. physics need center coordinate!
    Coordinates coords = positionToCoordinates(pos);
    coords.x += sizes[0] / 2;
    coords.y -= sizes[1] / 2;
    Rectangle rect(coords, sizes[0] / 2, sizes[1] / 2);
    DigitalVelocity vel(speedX, speedY);
    // hardcoded mass
    DynamicObject *obj = new DynamicObject(rect, 1, vel);


    setPhysicBody(obj);
}
SPlayer::~SPlayer()
{
    delete loader;
}

int SPlayer::getHP() const
{
    return HP;
}
int SPlayer::getWeaponID() const
{
    return weaponID;
}

void SPlayer::setWeaponID(int id)
{
    weaponID = id;
}

bool SPlayer::receiveDamage(int damage)
{
    HP -= damage;
    return HP <= 0;
}
