#include "game_objects/game_logger.h"
using namespace std;

// Logger
void ServerGameLogger::report_error(string s)
{
    errors.push_back(s);
}
void ServerGameLogger::print_errors_console()
{
    for (auto s: errors)
        cout << s;
}
