#include <cmath>
#include <iostream>

#include "game_objects/game_obj_structures.h"

using namespace std;
using namespace game_managers;

// GameMap
GameMap::GameMap(int mapID, ServerGameLogger *logger, MapLoader *loader): ID(mapID), cur_spawn(0),
                                                                          loader(loader)
{
    TiledMap_ObstaclesMatrix obstacles;
    std::vector<int> sizes;

    int err = loader->loadMap(mapID, sizes, spawnPoints, obstacles);
    if (err)
    {
        if (logger != nullptr)
            logger->report_error("<Error: failed to create map, default will be set>\n");
        obstacles.setSize(100, 100);
        spawnPoints.clear();
        sizes.resize(4);
        sizes[0] = sizes[1] = 10;
        sizes[2] = sizes[3] = 100;
        spawnPoints.push_back(Coordinates(500, 50));
        spawnPoints.push_back(Coordinates(100, 50));
    }

    tile.width = sizes[0];
    tile.height = sizes[1];

    width = sizes[2] * tile.width;
    height = sizes[3] * tile.height;

    physMap = new WorldTiledMap(tile, obstacles);
}
GameMap::~GameMap()
{
    delete physMap;
    delete loader;
}

int GameMap::getMapID()
{
    return ID;
}
int GameMap::getHeight()
{
    return height;
}
int GameMap::getWidth()
{
    return width;
}

WorldTiledMap* GameMap::getPhysMap()
{
    return physMap;
}
Coordinates GameMap::getPlayerSpawnCoord()
{
    Coordinates spawn = spawnPoints[cur_spawn];
    cur_spawn = (cur_spawn + 1) % spawnPoints.size();
    return spawn;
}
Coordinates GameMap::getWeaponSpawnCoord()
{
    Coordinates spawn;
    spawn.x = rand() % width;
    spawn.y = rand() % height;
    return spawn;
}


// Coordinate translator
CoordinateTranslator::CoordinateTranslator(): gameMap(nullptr) {}
CoordinateTranslator::CoordinateTranslator(GameMap *map): gameMap(map) {}

Coordinates CoordinateTranslator::positionToCoordinates(Position pos) const
{
    int h = gameMap->getHeight();
    return Coordinates(pos.x, h - pos.y);
}
Position CoordinateTranslator::coordinatesToPosition(Coordinates coords) const
{
    int h = gameMap->getHeight();
    return Position(coords.x, h - coords.y);
}

void CoordinateTranslator::setMap(GameMap *m)
{
    gameMap = m;
}
GameMap* CoordinateTranslator::getMap() const
{
    return gameMap;
}

void SpriteUpdater::updateSpriteTime(Sprite &sprite,  GameTime time)
{
    double frames = time / FRAME_SHIFT_TIME;
    sprite.frame = (long long)frames % sprite.frame_cnt;
}
