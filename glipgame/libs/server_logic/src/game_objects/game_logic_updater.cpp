#include <cmath>

#include "game_objects/game_logic_updater.h"

using namespace std;

// Logic updater
LogicUpdater::LogicUpdater(GameStorage* storage, PhysicWorld *world, IDTransformer *trans): storage(storage),
                                                                                            world(world),
                                                                                            trans(trans) {}
bool LogicUpdater::updateBullet(int bulletID, GameTime targetTime)
{
    SBullet* bullet = const_cast<SBullet*>((const SBullet*)storage->get(SBULLET, bulletID));
    GameTime dt = targetTime - bullet->getTime();
    bullet->updateTime(dt);

    // if bullet collides a map, it must be destroyed
    if (world->collisionManager->ifCollideMap(*bullet->getPhysicBody()))
        return true;

    // check all players for collision with bullet; deliver damage to them & destroy
    GameStorageIterator players;
    players.setIterator(storage, SPLAYER);
    SPlayer *player;

    while (!players.isDone())
    {
        player = const_cast<SPlayer*>((const SPlayer*)players.currentVal());
        if (world->collisionManager->ifCollide(*bullet->getPhysicBody(), *player->getPhysicBody()))
        {
            player->receiveDamage(bullet->getDamage());
            storage->update(SPLAYER, players.currentID(), player);
            return true;
        }
        players.next();
    }

    storage->update(SBULLET, bulletID, bullet);
    return false;
}
bool LogicUpdater::updateWeapon(int weaponID, GameTime targetTime)
{
    SWeapon* weapon = const_cast<SWeapon*>((const SWeapon*)storage->get(SWEAPON, weaponID));
    GameTime dt = targetTime - weapon->getTime();
    weapon->updateTime(dt);

    storage->update(SBULLET, weaponID, weapon);
    return false;
}
bool LogicUpdater::updatePlayer(int playerID, GameTime targetTime)
{
    SPlayer* player = const_cast<SPlayer*>((const SPlayer*)storage->get(SPLAYER, playerID));
    GameTime dt = targetTime - player->getTime();
    player->updateTime(dt);

    // if damage was received, here it'll be checked
    if (player->getHP() <= 0)
        return true;

    storage->update(SPLAYER, playerID, player);
    return false;
}
bool LogicUpdater::updatePlayerAction(int serverPlayerID, actionType action)
{
    // playerID is got as serverID, not storageID!!!
    int playerID = trans->getPlayerStorage(serverPlayerID);

    SPlayer* player = const_cast<SPlayer*>((const SPlayer*)storage->get(SPLAYER, playerID));
    if (!player)
        return false;
    if (action == LEFT_START)
    {
        player->getPhysicBody()->velocity.xVel = -player->getPhysicBody()->velocity.xVelValue;
        player->lookDirection = DIR_LEFT;
        // update action accordingly to its action type and weapon id
        player->sprite.action = actionTypetoID[LEFT_START]
                                + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);
    }
    else if (action == RIGHT_START)
    {
        player->getPhysicBody()->velocity.xVel = player->getPhysicBody()->velocity.xVelValue;
        player->lookDirection = DIR_RIGHT;
        player->sprite.action = actionTypetoID[RIGHT_START]
                                + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);
    }
    else if ((action == LEFT_END) || (action == RIGHT_END))
    {
        player->getPhysicBody()->velocity.xVel = 0;
        if (action == LEFT_END)
        {
            player->lookDirection = DIR_LEFT;
            player->sprite.action = actionTypetoID[LEFT_END]
                                    + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);
        }
        else
        {
            player->lookDirection = DIR_RIGHT;
            player->sprite.action = actionTypetoID[RIGHT_END]
                                    + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);
        }
    }
    else if (action == JUMP)
    {
        player->getPhysicBody()->velocity.yVel = player->getPhysicBody()->velocity.yVelValue;
        if (player->getVelocity().xVel >= 0)
            player->sprite.action = actionTypetoID[JUMP_LEFT]
                                    + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);

        else
            player->sprite.action = actionTypetoID[JUMP_RIGHT]
                                    + (player->getWeaponID() >= 0? (1+player->getWeaponID())*ACTIONS_CNT : 0);
    }
    else if (action == SSHOOT)
    {
        int weaponID = player->getWeaponID();
        if (weaponID >= 0) // is valid ID: player has weapon
        {
            SWeapon *weapon = const_cast<SWeapon*>((const SWeapon*)storage->get(SWEAPON, weaponID));
            Position shoot_pos = player->getPosition();
            shoot_pos.y += player->getPhysicBody()->getRectangle().height / 2;
            SBullet *bullet = weapon->shoot(player->lookDirection,
                                           player->getPhysicBody()->getRectangle().width/2 * 2,
                                           shoot_pos);
            if (bullet != nullptr) // weapon shooted a bullet
            {
                storage->add(SBULLET, bullet);
            }
            storage->update(SWEAPON, weaponID, weapon);
        }
    }
    else if (action == THROWWEAPON)
    {
        int weaponID = player->getWeaponID();
        if (weaponID >= 0) // is valid ID: player has weapon
        {
            SWeapon *weapon = const_cast<SWeapon*>((const SWeapon*)storage->get(SWEAPON, weaponID));
            weapon->onThrow(player->getPosition());
            storage->update(SWEAPON, weaponID, weapon);
        }
    }
    else if (action == PICKWEAPON)
    {
        int weaponID = player->getWeaponID();
        // TODO FEATURE: allow changing weapon on one button (without pushing 'throw' button)
        if (weaponID >= 0) // is valid ID: player has weapon, none can be picked
        {
            return false;
        }

        GameStorageIterator weapons;
        weapons.setIterator(storage, SWEAPON);
        SWeapon *weapon;

        // check all weapons: 1st found nearer then near_dist is picked up
        double near_dist = player->getPhysicBody()->getRectangle().width * PICKUP_NEAR_COEF; // semiwidth + eps
        while (!weapons.isDone())
        {
            weapon = const_cast<SWeapon*>((const SWeapon*)weapons.currentVal());
            if (world->collisionManager->ifNear(*weapon->getPhysicBody(), *player->getPhysicBody(), near_dist))
            {
                player->setWeaponID(weapons.currentID());
                weapon->onPickUp();
                storage->update(SWEAPON, weapons.currentID(), weapon);
                storage->update(SPLAYER, playerID, player);
                return false;
            }
            weapons.next();
        }

    }

    storage->update(SPLAYER, playerID, player);
    return false;
}


// ID transformer
IDTransformer::IDTransformer()
{
    IDs.clear();
}
void IDTransformer::addPlayer(int serverID, int storageID)
{
    IDs.push_back(make_pair(serverID, storageID));
}
void IDTransformer::removePlayer(int serverID)
{
    vector<pair<int, int>>::iterator iter = IDs.begin();
    for (; iter != IDs.end(); ++iter)
    {
        if (iter->first == serverID)
        {
            IDs.erase(iter);
            return;
        }
    }
}
int IDTransformer::getPlayerServer(int storageID)
{
    pair<int, int> p;
    for (int i = 0; i < IDs.size(); ++i)
    {
        p = IDs[i];
        if (p.second == storageID)
            return p.first;
    }
    return -1;
}
int IDTransformer::getPlayerStorage(int serverID)
{
    pair<int, int> p;
    for (int i = 0; i < IDs.size(); ++i)
    {
        p = IDs[i];
        if (p.first == serverID)
            return p.second;
    }
    return -1;
}

