#include "game_objects/game_managers.h"

using namespace std;
using namespace game_managers;

template <typename T>
static std::string toString(T val)
{
    std::ostringstream oss;
    oss << val;
    return oss.str();
}


//file manager
FileManager::FileManager()
{
    supportedLoadTypes.clear();
    prefixName.clear();
    suffixName.clear();

    supportedLoadTypes.insert(MAP_LOAD);
    supportedLoadTypes.insert(BULLET_LOAD);
    supportedLoadTypes.insert(WEAPON_LOAD);
    supportedLoadTypes.insert(PLAYER_LOAD);
    supportedLoadTypes.insert(SPRITE_LOAD);

    prefixName[MAP_LOAD] = "maps/map_";
    suffixName[MAP_LOAD] = ".map";

    prefixName[BULLET_LOAD] = "bullets/bullet_";
    suffixName[BULLET_LOAD] = ".bullet";

    prefixName[WEAPON_LOAD] = "weapons/weapon_";
    suffixName[WEAPON_LOAD] = ".weapon";

    prefixName[PLAYER_LOAD] = "players/player_";
    suffixName[PLAYER_LOAD] = ".player";

    prefixName[SPRITE_LOAD] = "sprites/sprite_";
    suffixName[SPRITE_LOAD] = ".sprite";
}

bool FileManager::isSupported(LoadType type)
{
    return supportedLoadTypes.count(type) != 0;
}
string FileManager::getFilename(LoadType type, int ID)
{
    if (!isSupported(type))
        return string();

    //SERVER_LOGIC_STATIC_PATH is defined in cmake
    return SERVER_LOGIC_STATIC_PATH + prefixName[type] + toString(ID) + suffixName[type];
}


// file loaders
int SpriteLoaderFile::loadSprite(int spriteID, int &frameCnt, vector<int> &sizes)
{
    string filename = getFilename(SPRITE_LOAD, spriteID);
    ifstream infile;
    infile.open(filename);

    if (!infile.is_open())
        return LOAD_STATIC_ERROR;

    sizes.resize(2);
    infile >> frameCnt >> sizes[0] >> sizes[1];
    infile.close();

    return LOAD_STATIC_SUCCESS;
}

int MapLoaderFile::loadMap(int mapID, vector<int> &sizes,
                          vector<Coordinates> &spawn,
                          TiledMap_ObstaclesMatrix &obstacles)
{
    string filename = getFilename(MAP_LOAD, mapID);
    ifstream infile;
    infile.open(filename);

    if (!infile.is_open())
        return LOAD_STATIC_ERROR;

    // load sizes: tile's width, height; map's width, height (in tiles)
    sizes.resize(4);
    for (int i = 0; i < 4; ++i)
        infile >> sizes[i];

    int width = sizes[2], height = sizes[3];

    spawn.clear();
    Coordinates coord;

    // load obstacle map (boolean matrix)
    // coming at 'left-up' zero, processed as standart coords
    obstacles.setSize(height, width);
    int tmp;
    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            infile >> tmp;
            if (tmp == 1)
                obstacles.set(height -1 - i, j, true);
            else
            {
                if (tmp == 2) // tile is a spawn point
                {
                    coord.y = (height - 1 - i + 0.5) * sizes[0];
                    coord.x = (j + 0.5) * sizes[1];
                    spawn.push_back(coord);
                }
                obstacles.set(height -1 - i, j, false);
            }
        }
    }

    infile.close();
    return LOAD_STATIC_SUCCESS;
}
int BulletLoaderFile::loadBullet(int bulletID, int &spriteID, int &frameCnt,
               double &velocity, int &damage, vector<int> &sizes)
{

    string filename = getFilename(BULLET_LOAD, bulletID);
    ifstream infile;
    infile.open(filename);

    if (!infile.is_open())
        return LOAD_STATIC_ERROR;
    infile >> spriteID >> velocity >> damage;
    infile.close();

    if (loadSprite(spriteID, frameCnt, sizes))
        return LOAD_STATIC_ERROR;

    return LOAD_STATIC_SUCCESS;
}


int WeaponLoaderFile::loadWeapon(int weaponID, int &spriteID, int &frameCnt,
               int &bulletID, int &capacity, double &firespeed, vector<int> &sizes)
{

    string filename = getFilename(WEAPON_LOAD, weaponID);
    ifstream infile;
    infile.open(filename);

    if (!infile.is_open())
        return LOAD_STATIC_ERROR;
    infile >> spriteID >> bulletID >> capacity >> firespeed;
    infile.close();

    if (loadSprite(spriteID, frameCnt, sizes))
        return LOAD_STATIC_ERROR;

    return LOAD_STATIC_SUCCESS;
}

int PlayerLoaderFile::loadPlayer(int playerID, int &spriteID, int &frameCnt,
               int &HP, double &speedX, double &speedY, vector<int> &sizes)
{

    string filename = getFilename(PLAYER_LOAD, playerID);
    ifstream infile;
    infile.open(filename);

    if (!infile.is_open())
        return LOAD_STATIC_ERROR;

    infile >> spriteID >> HP >> speedX >> speedY;
    infile.close();

    if (loadSprite(spriteID, frameCnt, sizes))
        return LOAD_STATIC_ERROR;

    return LOAD_STATIC_SUCCESS;
}
