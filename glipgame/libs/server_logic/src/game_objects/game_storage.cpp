#include <cmath>

#include "game_objects/game_storage.h"

using namespace std;

// Game Storage

struct GameStorageData
{
    std::unordered_map<int, SGameObject*> bullets;
    std::unordered_map<int, SGameObject*> weapons;
    std::unordered_map<int, SGameObject*> players;

    ~GameStorageData()
    {
        SGameObject *obj;
        while (!players.empty())
        {
            obj = players.begin()->second;
            players.erase(players.begin());
            delete obj;
        }
        while (!weapons.empty())
        {
            obj = weapons.begin()->second;
            weapons.erase(weapons.begin());
            delete obj;
        }
        while (!bullets.empty())
        {
            obj = bullets.begin()->second;
            bullets.erase(bullets.begin());
            delete obj;
        }
    }
};

GameStorage::GameStorage(): data(new GameStorageData()) {}
GameStorage::~GameStorage()
{
    delete data;
}
unordered_map<int, SGameObject*> * GameStorage::get_container(objectType type)
{
    if (type == SBULLET)
        return &data->bullets;
    else if (type == SWEAPON)
        return &data->weapons;
    else
        return &data->players;
}

int GameStorage::add(objectType type, SGameObject *object)
{
    int key;
    pair<unordered_map<int, SGameObject*>::iterator, bool> ret_pair;
    unordered_map<int, SGameObject*> *container = get_container(type);

    std::mt19937 gen(time(0));
    std::uniform_int_distribution<> key_gen(0, INT_MAX);

    while (ret_pair.second == false)
    {
        key = key_gen(gen);
        ret_pair = container->insert(make_pair(key, object));
    }
    return key;
}
int GameStorage::remove(objectType type, int id)
{
    unordered_map<int, SGameObject*> *container = get_container(type);

    if (container->find(id) != container->end())
    {
        delete (*container)[id];
        container->erase(id);
        return 0;
    }
    return 1;
}
int GameStorage::update(objectType type, int id, SGameObject *object)
{
    unordered_map<int, SGameObject*> *container = get_container(type);

    if (container->find(id) != container->end())
    {
        (*container)[id] = object;
        return 0;
    }
    return 1;
}
const SGameObject* GameStorage::get(objectType type, int id)
{
    unordered_map<int, SGameObject*> *container = get_container(type);

    if (container->find(id) != container->end())
    {
        return (*container)[id];
    }
    return nullptr;
}


// Game Storage Iterator
struct GameStorageIteratorData
{
    GameStorage *storage;
    std::unordered_map<int, SGameObject*> *container;
    typename std::unordered_map<int, SGameObject*>::iterator iter;
    objectType type;

    GameStorageIteratorData(): storage(nullptr) {}
};

GameStorageIterator::GameStorageIterator(): data(new GameStorageIteratorData()) {}
GameStorageIterator::~GameStorageIterator()
{
    delete data;
}

void GameStorageIterator::setIterator(GameStorage *st, objectType tp)
{
    data->storage = st;
    data->type = tp;

    data->container = data->storage->get_container(data->type);
    data->iter = data->container->begin();
}
const SGameObject* GameStorageIterator::set_first()
{
    data->iter = data->container->begin();
    return data->iter->second;

}

void GameStorageIterator::next()
{
    data->iter++;
}
bool GameStorageIterator::isDone()
{
    return data->iter == data->container->end();
}
const SGameObject* GameStorageIterator::currentVal()
{
    return data->iter->second;
}
int GameStorageIterator::currentID()
{
    return data->iter->first;
}
