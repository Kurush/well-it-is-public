#include "physics/phys_dynamic_objects.h"

using namespace std;

// Time updater
bool TimeUpdater::timeUpdate(DynamicObject *obj, GameTime dt, double gravity)
{
    if (dt < GAME_TIME_EPS)
        return false;

    obj->rect.coords.x += dt * obj->velocity.xVel;
    double accel = gravity * obj->getMass();


    obj->velocity.yVel += accel * dt;
    int sign = obj->velocity.yVel>0?1:-1;
    obj->velocity.yVel = min(fabs(obj->velocity.yVel), MAX_POSSIBLE_YSPEED) * sign;

    obj->rect.coords.y += dt * obj->velocity.yVel;
    return true;
}

// Dynamic object
DynamicObject::DynamicObject(InterfaceTimeUpdater *upd /*= new TimeUpdater()*/)
{
    PhysicObject();
    velocity = DigitalVelocity();
    timeUpdater = upd;
}

DynamicObject::DynamicObject(const DynamicObject &obj): PhysicObject(obj.getRectangle(), obj.getMass()),
                                                        velocity(obj.velocity)
{
    timeUpdater = new TimeUpdater;
}
DynamicObject::DynamicObject(Rectangle rect, double mass,
                             double xVelVal, double yVelVal,
                             InterfaceTimeUpdater *upd /*= new TimeUpdater()*/): PhysicObject(rect, mass),
                                                                        velocity(xVelVal, yVelVal),
                                                                        timeUpdater(upd) {}
DynamicObject::DynamicObject(Rectangle rect, double mass,
                             DigitalVelocity vel,
                             InterfaceTimeUpdater *upd /*= new TimeUpdater()*/): PhysicObject(rect, mass),
                                                                        velocity(vel),
                                                                        timeUpdater(upd) {}
DynamicObject::~DynamicObject()
{
    delete timeUpdater;
}
bool DynamicObject::updateTime(GameTime dt, double gravity)
{
    return timeUpdater->timeUpdate(this, dt, gravity);
}
bool DynamicObject::operator == (const DynamicObject &right) const
{
    return tie(rect, mass, velocity) == tie(right.rect, right.mass, right.velocity);
}
