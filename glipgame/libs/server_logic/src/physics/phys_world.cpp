#include "physics/phys_world.h"

using namespace std;

// Physic World
struct PWorldContents
{
    double gravity;
    unordered_map<int, DynamicObject*> dynObjects;
    WorldTiledMap *worldMap;

    PWorldContents(): gravity(0.), worldMap(nullptr){}

    bool operator == (const PWorldContents &right) const
    {
        return (gravity == right.gravity) && (dynObjects == right.dynObjects) &&
               (worldMap == right.worldMap);
    }


};

PhysicWorld::PhysicWorld(double gravity)
{
    contents = new PWorldContents;
    contents->gravity = gravity;
    collisionManager = nullptr;
}
PhysicWorld::PhysicWorld()
{
    contents = new PWorldContents;
    collisionManager = nullptr;
}
PhysicWorld::~PhysicWorld()
{
    delete contents;
    if (collisionManager)
        delete collisionManager;
}

int PhysicWorld::getGravity()
{
    return contents->gravity;
}
WorldTiledMap* PhysicWorld::getMap()
{
    return this->contents->worldMap;
}
int PhysicWorld::hasMap()
{
    return contents->worldMap != nullptr;
}
int PhysicWorld::objectsCount()
{
    return contents->dynObjects.size();
}

void PhysicWorld::setCollisionManager(InterfaceCollisionManager *mng)
{
    collisionManager = mng;
}
int PhysicWorld::addDynamicObject(DynamicObject *obj)
{
    std::mt19937 gen(time(0));
    std::uniform_int_distribution<> key_gen(0, INT_MAX);

    int key = key_gen(gen);
    pair<unordered_map<int, DynamicObject*>::iterator, bool> ret_pair;
    ret_pair = contents->dynObjects.insert(make_pair(key, obj));

    while (ret_pair.second == false)
    {
        key = key_gen(gen);
        ret_pair = contents->dynObjects.insert(make_pair(key, obj));
    }

    return key;
}
void PhysicWorld::addWorldMap(WorldTiledMap *wMap)
{
    contents->worldMap = wMap;
}

void PhysicWorld::removeDynamicObject(int key)
{
    if (contents->dynObjects.count(key))
        contents->dynObjects.erase(key);
}
void PhysicWorld::removeWorldMap()
{
    contents->worldMap = nullptr;
}

void PhysicWorld::UpdateTime(GameTime dt)
{
    unordered_map<int, DynamicObject*>::iterator iter = contents->dynObjects.begin();
    while (iter != contents->dynObjects.end())
    {
        iter->second->updateTime(dt, contents->gravity);
        ++iter;
    }

}
// algorithm may be improved
void PhysicWorld::UpdateCollisions()
{
    unordered_map<int, DynamicObject*>::iterator iter = contents->dynObjects.begin(), iter1;
    int cycle_cnt = 3;

    for (int i = 0; i < cycle_cnt; ++i)
    {
        while (iter != contents->dynObjects.end()) // static collisions
        {
            DynamicObject *obj = iter->second;
            WorldTiledMap *map = contents->worldMap;
            collisionManager->resolveCollisionMap(*map, *obj);
            ++iter;
        }

        iter = contents->dynObjects.begin();
        while (iter != contents->dynObjects.end()) // dynamic collisions
        {
            iter1 = contents->dynObjects.begin();
            while (iter1 != iter)
                ++iter1;
            ++iter1;
            while (iter1 != contents->dynObjects.end())
            {
                collisionManager->resolveCollisionDD(*(iter->second), *(iter1->second));
                ++iter1;
            }
            ++iter;
        }
    }
}

bool PhysicWorld::operator == (const PhysicWorld &right) const
{
    return (contents == right.contents);
}

