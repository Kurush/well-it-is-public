#include "physics/phys_collision_manager.h"
#include "physics/phys_world.h"

using namespace std;

// Collision checker
bool CollisionChecker::ifCollide(PhysicObject obj1, PhysicObject obj2)
{
    Rectangle rect1 = obj1.getRectangle();
    Rectangle rect2 = obj2.getRectangle();

    if (fabs(rect1.coords.x - rect2.coords.x) < rect1.width + rect2.width - COORDINATES_EPS)
        if (fabs(rect1.coords.y - rect2.coords.y) < rect1.height + rect2.height - COORDINATES_EPS)
            return true;
    return false;
}
bool CollisionChecker::ifCollideLeft(PhysicObject obj1, PhysicObject obj2)
{
    Rectangle rect1 = obj1.getRectangle();
    Rectangle rect2 = obj2.getRectangle();
    double val = rect2.coords.x - rect1.coords.x;
    if ((val > -COORDINATES_EPS) && (val < rect1.width + rect2.width - COORDINATES_EPS))
            return true;
    return false;
}
bool CollisionChecker::ifCollideRight(PhysicObject obj1, PhysicObject obj2)
{
    Rectangle rect1 = obj1.getRectangle();
    Rectangle rect2 = obj2.getRectangle();

    double val = rect2.coords.x - rect1.coords.x;
    if ((val < COORDINATES_EPS) && (-val < rect1.width + rect2.width - COORDINATES_EPS))
            return true;
    return false;
}
bool CollisionChecker::ifCollideUp(PhysicObject obj1, PhysicObject obj2)
{
    Rectangle rect1 = obj1.getRectangle();
    Rectangle rect2 = obj2.getRectangle();

    double val = rect1.coords.y - rect2.coords.y;
    if ((val > -COORDINATES_EPS) && (val < rect1.height + rect2.height - COORDINATES_EPS))
            return true;
    return false;
}
bool CollisionChecker::ifCollideDown(PhysicObject obj1, PhysicObject obj2)
{
    Rectangle rect1 = obj1.getRectangle();
    Rectangle rect2 = obj2.getRectangle();

    double val = rect2.coords.y - rect1.coords.y;
    if ((val > -COORDINATES_EPS) && (val < rect1.height + rect2.height - COORDINATES_EPS))
            return true;
    return false;
}
bool CollisionChecker::ifNear(PhysicObject obj1, PhysicObject obj2, double len)
{
    Coordinates c1 = obj1.getRectangle().coords;
    Coordinates c2 = obj2.getRectangle().coords;
    double dist = (c1.x - c2.x) * (c1.x - c2.x) + (c1.y - c2.y) * (c1.y - c2.y);
    return dist <= len*len;
}


// Interface collision manager
InterfaceCollisionManager::InterfaceCollisionManager(PhysicWorld *w): world(w) {}


// Collision manager
CollisionManager::CollisionManager(PhysicWorld *w): InterfaceCollisionManager(w) {}
bool CollisionManager::ifCollideMap(PhysicObject obj1)
{
    for (size_t i = 0; i < world->getMap()->obstacles.size(); ++i)
    {
        if (ifCollide(obj1, world->getMap()->obstacles[i]))
            return true;
    }
    return false;
}
int CollisionManager::resolveCollisionSD(StaticObject &objS, DynamicObject &objD)
{
    if (!ifCollide(objS, objD))
        return 0;
    Rectangle rect_s = objS.getRectangle();
    Rectangle rect_d = objD.getRectangle();

    double width = 0, height = 0;
    if (fabs(rect_s.coords.x - rect_d.coords.x) < rect_s.width + rect_d.width - COORDINATES_EPS)
        width = (-fabs(rect_s.coords.x - rect_d.coords.x) +
                     rect_s.width + rect_d.width);
    if (fabs(rect_s.coords.y - rect_d.coords.y) < rect_s.height + rect_d.height - COORDINATES_EPS)
        height = (-fabs(rect_s.coords.y - rect_d.coords.y) +
                     rect_s.height + rect_d.height);

    // dyn.object is to the left
    if (width < height + COORDINATES_EPS)
    {
        if (ifCollideLeft(objD, objS))
        {
            objD.velocity.xVel = 0;
            Coordinates coords;
            coords.x = rect_s.coords.x - rect_s.width - rect_d.width;
            coords.y = rect_d.coords.y;
            objD.setCoordinates(coords);
        }
        else if (ifCollideRight(objD, objS))
        {
            objD.velocity.xVel = 0;
            Coordinates coords;
            coords.x = rect_s.coords.x + rect_s.width + rect_d.width;
            coords.y = rect_d.coords.y;
            objD.setCoordinates(coords);
        }
    }
    else
    {
        if (ifCollideUp(objD, objS))
        {
            objD.velocity.yVel = 0;
            Coordinates coords;
            coords.x = rect_d.coords.x;
            coords.y = rect_s.coords.y + rect_s.height + rect_d.height;
            objD.setCoordinates(coords);
        }
        else if (ifCollideDown(objD, objS))
        {
            objD.velocity.yVel = 0;
            Coordinates coords;
            coords.x = rect_d.coords.x;
            coords.y = rect_s.coords.y - rect_s.height - rect_d.height;
            objD.setCoordinates(coords);
        }
    }
    return 1;
}
int CollisionManager::resolveCollisionDD(DynamicObject &obj1, DynamicObject &obj2)
{
    if (!ifCollide(obj1, obj2))
        return 0;
    Rectangle rect2 = obj2.getRectangle();
    Rectangle rect1 = obj1.getRectangle();

    double width = 0, height = 0;
    if (fabs(rect1.coords.x - rect2.coords.x) < rect1.width + rect2.width - COORDINATES_EPS)
        width = (-fabs(rect1.coords.x - rect2.coords.x) +
                     rect1.width + rect2.width);
    if (fabs(rect1.coords.y - rect2.coords.y) < rect1.height + rect2.height - COORDINATES_EPS)
        height = (-fabs(rect1.coords.y - rect2.coords.y) +
                     rect1.height + rect2.height);

    if (width < height + COORDINATES_EPS)
    {
        bool swap_flag = 0;
        DynamicObject *obj1_p = &obj1;
        DynamicObject *obj2_p = &obj2;
        if (ifCollideRight(obj1, obj2)) // right case is left case with swapped objects
        {
            swap_flag = 1; // to do 'closing' swap after colliding
            obj1_p = &obj2;
            obj2_p = &obj1;
            swap(rect1, rect2);
        }
        // 1st object is to the left
        if (ifCollideLeft(*obj1_p, *obj2_p))
        {
            double vel1 = obj1_p->velocity.xVel, vel2 = obj2_p->velocity.xVel;
            double contact_x, semiwidth; // center of contact; semiwidth of contact zone;
            semiwidth = (-fabs(rect2.coords.x - rect1.coords.x) +
                         rect1.width + rect2.width) / 2;
            contact_x = rect1.coords.x + rect1.width - semiwidth * 2;

            if ((vel1 == 0) && (vel2 == 0)) // * *
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth, rect1.coords.y));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth, rect2.coords.y));
            }
            else if ((vel1 == 0) && (vel2 > 0)) // * *->
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth * 2, rect2.coords.y));
            }
            else if ((vel1 == 0) && (vel2 < 0)) // * <-*
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth * 2, rect1.coords.y));
            }
            else if ((vel1 < 0) && (vel2 == 0)) // <-* *
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth * 2, rect1.coords.y));
            }
            else if ((vel1 < 0) && (vel2 > 0)) // <-* *->
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth, rect1.coords.y));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth, rect2.coords.y));
            }
            else if ((vel1 < 0) && (vel2 < 0)) // <-* <-* non dependent to speed values!
            {
                    obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth * 2, rect1.coords.y));
            }
            else if ((vel1 > 0) && (vel2 == 0)) // *-> *
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth * 2, rect2.coords.y));
            }
            else if ((vel1 > 0) && (vel2 > 0)) // *-> *->
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth * 2, rect2.coords.y));
            }
            else if ((vel1 > 0) && (vel2 < 0)) // *-> <-*
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x - semiwidth, rect1.coords.y));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x + semiwidth, rect2.coords.y));
                obj1_p->velocity.xVel = 0;
                obj2_p->velocity.xVel = 0;
            }

            /*if (swap_flag)
            {
                swap(obj1, obj2);
            }*/
       }
    }
    else
    {
        bool swap_flag = 0;
        DynamicObject *obj1_p = &obj1;
        DynamicObject *obj2_p = &obj2;
        if (ifCollideUp(obj1, obj2)) // up case is left case with swapped objects
        {
            swap_flag = 1; // to do 'closing' swap after colliding
            obj1_p = &obj2;
            obj2_p = &obj1;
            swap(rect1, rect2);
        }
        if (ifCollideDown(*obj1_p, *obj2_p))
        {
            double vel1 = obj1_p->velocity.yVel, vel2 = obj2_p->velocity.yVel;
            double contact_y, semiheight; // center of contact; semiheight of contact zone;
            semiheight = (-fabs(rect2.coords.y - rect1.coords.y) +
                         rect1.height + rect2.height) / 2;
            contact_y = rect1.coords.y + rect1.height - semiheight * 2;

            if ((vel1 == 0) && (vel2 == 0)) // * *
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight));
            }
            else if ((vel1 == 0) && (vel2 > 0)) // * *->
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight * 2));
            }
            else if ((vel1 == 0) && (vel2 < 0)) // * <-*
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight * 2));
            }
            else if ((vel1 < 0) && (vel2 == 0)) // <-* *
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight * 2));
            }
            else if ((vel1 < 0) && (vel2 > 0)) // <-* *->
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight));
            }
            else if ((vel1 < 0) && (vel2 < 0)) // <-* <-*
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight * 2));
            }
            else if ((vel1 > 0) && (vel2 == 0)) // *-> *
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight * 2));
            }
            else if ((vel1 > 0) && (vel2 > 0)) // *-> *->
            {
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight * 2));
            }
            else if ((vel1 > 0) && (vel2 < 0)) // *-> <-*
            {
                obj1_p->setCoordinates(Coordinates(rect1.coords.x, rect1.coords.y - semiheight));
                obj2_p->setCoordinates(Coordinates(rect2.coords.x, rect2.coords.y + semiheight));
                obj1_p->velocity.yVel = 0;
                obj2_p->velocity.yVel = 0;
            }
       }
    }
    return 1;
}
int CollisionManager::resolveCollisionMap(WorldTiledMap map, DynamicObject &obj)
{
    int collided = 0;
    for (size_t i = 0; i < map.obstacles.size(); ++i)
    {
        collided |= resolveCollisionSD(map.obstacles[i], obj);
    }
    return collided;
}
