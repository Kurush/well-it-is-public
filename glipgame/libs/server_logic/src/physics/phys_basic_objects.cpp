#include "physics/phys_basic_objects.h"

using namespace std;

// tiled map matrix
bool TiledMap_ObstaclesMatrix::isValidIndexes(int i, int j)
{
    if (matrix.empty())
        return false;
    if ((i >= 0) && (i <= matrix.size()) &&
        (j >= 0) && (j <= matrix[0].size()))
    {
        return true;
    }
    return false;
}
bool TiledMap_ObstaclesMatrix::get(int i, int j)
{
    if (!isValidIndexes(i, j))
        return false;
    return matrix[i][j];
}
void TiledMap_ObstaclesMatrix::set(int i, int j, bool val)
{
    if (isValidIndexes(i, j))
        matrix[i][j] = val;
}
void TiledMap_ObstaclesMatrix::setSize(int n, int m)
{
    matrix.resize(n, vector<bool>(m, false));
}
int TiledMap_ObstaclesMatrix::rowCnt()
{
    return matrix.size();
}
int TiledMap_ObstaclesMatrix::colCnt()
{
    if (!matrix.size())
        return 0;
    return matrix[0].size();
}


// PhysicObject
PhysicObject::PhysicObject(Rectangle rect, double mass): rect(rect), mass(mass) {}
PhysicObject::PhysicObject(): mass(DEFAULT_MASS) {}
PhysicObject::PhysicObject(const PhysicObject &obj): rect(obj.getRectangle()), mass(obj.getMass()) {}
Coordinates PhysicObject::getCoordinates() const
{
    return rect.coords;
}
void PhysicObject::setCoordinates(Coordinates c)
{
    rect.coords = c;
}

Rectangle PhysicObject::getRectangle() const
{
    return rect;
}
double PhysicObject::getMass() const
{
    return mass;
}


// Static object
StaticObject::StaticObject(Rectangle rect): PhysicObject(rect, STATIC_MASS_INF) {}
StaticObject::StaticObject(): PhysicObject(Rectangle(), STATIC_MASS_INF) {}
StaticObject::StaticObject(const StaticObject &obj)
{
    rect = obj.getRectangle();
    mass = STATIC_MASS_INF;
}


// World Tiled Map
WorldTiledMap::WorldTiledMap()
{
    obstacles.clear();
}

// may be optimized for multiline obstacle search
WorldTiledMap::WorldTiledMap(Rectangle tile, TiledMap_ObstaclesMatrix matrix)
{
    // comming matrix: in classic coordinates (m[0][0] is left-down tile with (0,0) left-down corner)

    obstacles.clear();
    size_t n = matrix.rowCnt();
    size_t m = matrix.colCnt(); // map must be rectangular!

    // add borders
    Coordinates c(-32, n * tile.height / 2);
    Rectangle r(c, 32, n * tile.height / 2);
    StaticObject o(r);
    obstacles.push_back(o); // left border

    c = Coordinates(m * tile.width + 32, n * tile.height / 2);
    r = Rectangle(c, 32, n * tile.height / 2);
    o = StaticObject(r);
    obstacles.push_back(o); // right border

    c = Coordinates(m * tile.width / 2, -32);
    r = Rectangle(c, m * tile.width / 2, 32);
    o = StaticObject(r);
    obstacles.push_back(o); // down border

    c = Coordinates(m * tile.width / 2, n * tile.height + 32);
    r = Rectangle(c, m * tile.width / 2, 32);
    o = StaticObject(r);
    obstacles.push_back(o); // up border

    for (size_t i = 0; i < n; ++i)
    {
        size_t left = -1, right = -1;
        for (size_t j = 0; j < m; ++j)
        {
            if (matrix.get(i, j) == true) // is obstacle
            {
                if (left == -1) // obstacle started
                {
                    left = j;
                }
                if ((j == m - 1) || (matrix.get(i, j+1) == false)) // obst ended
                {
                    right = j - 1;
                    double len_x = right - left + 1;
                    double center_x = left + len_x / 2;
                    double center_y = i + 0.5;
                    Coordinates center(center_x * tile.width, center_y * tile.height);
                    Rectangle rect(center, len_x * tile.width / 2, tile.height / 2);
                    StaticObject obst(rect);
                    obstacles.push_back(obst);
                    left = -1;
                }
            }
            else
            {
                left = -1;
            }
        }
    }
}





