#include "physics/phys_structures.h"

using namespace std;

// Coordinates
Coordinates::Coordinates(): x(0.), y(0.) {}
Coordinates::Coordinates(const Coordinates &c): x(c.x), y(c.y) {}
Coordinates::Coordinates(double x, double y): x(x), y(y) {}
bool Coordinates::operator == (const Coordinates &right) const
{
    return (x == right.x) && (y == right.y);
}


// Rectangle
Rectangle::Rectangle(): coords(), width(1), height(1) {}
Rectangle::Rectangle(const Rectangle &r): coords(r.coords), width(r.width), height(r.height) {}
Rectangle::Rectangle(Coordinates coords, double w, double h): coords(coords), width(w), height(h) {}
Rectangle::Rectangle(double x, double y, double w, double h): coords(x, y), width(w), height(h) {}
Rectangle::Rectangle(double w, double h): coords(), width(w), height(h) {}
bool Rectangle::operator == (const Rectangle &right) const
{
    return tie(coords, width, height) == tie(right.coords, right.width, right.height);
}


// DigitalVelocity
DigitalVelocity::DigitalVelocity(): xVel(0.), yVel(0.), xVelValue(0.), yVelValue(0.) {}
DigitalVelocity::DigitalVelocity(double xVal, double yVal): xVel(0.), yVel(0.),
                                                            xVelValue(xVal), yVelValue(yVal) {}
DigitalVelocity::DigitalVelocity(double x, double y,
                                 double xVal, double yVal): xVel(x), yVel(y),
                                                            xVelValue(xVal), yVelValue(yVal) {}
DigitalVelocity::DigitalVelocity(const DigitalVelocity& vel): xVel(vel.xVel), yVel(vel.yVel),
                                                              xVelValue(vel.xVelValue), yVelValue(vel.yVelValue) {}
bool DigitalVelocity::operator == (const DigitalVelocity &right) const
{
    return tie(xVel, yVel, xVelValue, yVelValue) ==
           tie(right.xVel, right.yVel, right.xVelValue, right.yVelValue);
}






