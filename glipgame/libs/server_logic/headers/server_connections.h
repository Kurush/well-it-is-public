#ifndef SERVER_CONNECTIONS_H
#define SERVER_CONNECTIONS_H

#include "packages.h"
#include "game_objects/game_logic_updater.h"
#include "connector.h"

/// \brief Parser for client packages
class ClientPackageHandler
{
public:
    ClientPackageHandler(LogicUpdater *upd);
    std::shared_ptr<IMessage> eventHandler(std::shared_ptr<IMessage> message);
    bool isSupported(client_packages::Commands type);
private:
    LogicUpdater *updater;
    /// translator from client action-type names to server's
    std::map<client_packages::Commands, actionType> command_interpret;
    std::set<client_packages::Commands> supportedActionTypes;
};

class ServerGame;
/// \brief Parser for adding / deleting players
class PlayersHandler
{
public:
    PlayersHandler(ServerGame *game);
    std::shared_ptr<IMessage> addPlayerHandler(std::shared_ptr<IMessage> message);
    std::shared_ptr<IMessage> removePlayerHandler(std::shared_ptr<IMessage> message);
private:
    ServerGame *game;
};

/// \brief Collect and send data to clients
/**
  Provides function for:
  - sending packages for all clients
  - preparing a common package
  - specifying ans sending package for defined client
 */
class PackageSender
{
public:
    PackageSender(IConnector *connector, GameStorage *storage, IDTransformer *transformer);
    void sendUpdatePackages(GameTime gameTime);
    void sendInitPackage(int serverID, int playerID, int mapID, GameTime gameTime);
    IConnector *connector;
private:
    GameStorage *storage;
    IDTransformer *transformer;
};


/// \brief Container of classes for managing packeges
class PackageManagers
{
public:
     PackageManagers(IConnector *connector, GameStorage *storage, IDTransformer *transformer, LogicUpdater *upd,
                     ServerGame* game);
     ~PackageManagers();

     ClientPackageHandler *inputHandler;
     PackageSender *outputManager;
     PlayersHandler *playerHandler;
};



#endif // SERVER_CONNECTIONS_H
