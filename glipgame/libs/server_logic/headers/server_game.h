#ifndef SERVER_GAME_H
#define SERVER_GAME_H

#include <memory>
#include <random>
#include <string>

#include "game_objects/game_logger.h"
#include "game_objects/game_storage.h"
#include "physics/phys_world.h"
#include "server_connections.h"

const int PLAYER_CNT = 1;
const int MAP_CNT = 1;
const int  WEAPONS_CNT = 1;

const int MIN_WEPONS_ON_MAP = 2;
const int MAX_WEPONS_ON_MAP = 7;

const double TIME_UPDATE_STEP = 0.05;


struct ServerGameData;
/// \brief Server game logic
/**
  The 'Game' object is responsable for storage and regular updates
  of all game objects (maps, players, all other objects); after each
  general update (fucntion 'update') sends data to all current players
  ('Actors', who are present in 'gameActors' attribute).
  Update by a time 'dt' means moving objects, than checking and resolving collisions.
  Also partial update takes place when the action package comes from client(-s).

  When initialized, a game starts. Clients may be added/removed dynamically;
  when new client is added, server sends "initial" package:
  player game ID, map ID and some other data;
 */
class ServerGame
{
public:
    ServerGame(IConnector *connector);
    ~ServerGame();
    /// updates both time and logic (physics included), then sends packages of updated game to all players
    void update(GameTime dt);
    void addPlayer(int serverID);
    void removePlayer(int serverID);

protected:
    ServerGameData *data;

    void addWeapon(int id = -1);

    void updateBullets(GameTime dt, std::queue<std::pair<objectType, int>> &toDelete);
    void updateWeapons(GameTime dt, std::queue<std::pair<objectType, int>> &toDelete);
    void updatePlayers(GameTime dt, std::queue<std::pair<objectType, int>> &toDelete);

};

#endif // SERVER_GAME_H
