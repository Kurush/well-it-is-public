#ifndef PHYS_DYNAMIC_OBJECTS_H
#define PHYS_DYNAMIC_OBJECTS_H

#include <cmath>

#include "physics/phys_basic_objects.h"

///\authors Kazakov Kirill

const double MAX_POSSIBLE_YSPEED = 100.;

class DynamicObject;
class InterfaceTimeUpdater
{
public:
    virtual bool timeUpdate(DynamicObject *obj, GameTime dt, double gravity) = 0;
    virtual ~InterfaceTimeUpdater() {}
};

class TimeUpdater: public InterfaceTimeUpdater
{
public:
    virtual bool timeUpdate(DynamicObject *obj, GameTime dt, double gravity);
};

/// \brief Class for dynamic objects
/**
  Dynamic object is the object with ability to move
  (speed only, no accelerations except for gravity).
 */
class DynamicObject: public PhysicObject
{
public:
    DynamicObject(InterfaceTimeUpdater *upd = new TimeUpdater());
    DynamicObject(const DynamicObject &obj);
    DynamicObject(Rectangle rect, double mass,
                  double xVelVal, double yVelVal, InterfaceTimeUpdater *upd = new TimeUpdater());
    DynamicObject(Rectangle rect, double mass,
                  DigitalVelocity velocity, InterfaceTimeUpdater *upd = new TimeUpdater());
    ~DynamicObject();

    /// uses time updater to change velocities and coordinates
    bool updateTime(GameTime dt, double gravity);
    bool operator == (const DynamicObject &right) const;

    DigitalVelocity velocity;
    InterfaceTimeUpdater *timeUpdater;
    friend bool TimeUpdater::timeUpdate(DynamicObject *, GameTime, double);
};


#endif // PHYS_DYNAMIC_OBJECTS_H
