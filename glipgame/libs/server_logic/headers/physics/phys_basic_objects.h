#ifndef PHYS_BASIC_OBJECTS_H
#define PHYS_BASIC_OBJECTS_H

///\authors Kazakov Kirill

#include <map>
#include <stdlib.h>
#include <vector>

#include "phys_structures.h"

typedef double GameTime;

/// \brief Class that represents obstacle matrix
/**
  Essentially, that's a boolean matrix with:
  -  matrix[i][j] = true, if a tile on i-th row, j-th column is an obstacle
  -  matrix[i][j] = false, othewise
 */
class TiledMap_ObstaclesMatrix
{
public:
    bool get(int i, int j);
    void set(int i, int j, bool val);
    /// ol values deleted; all new values are set FALSE
    void setSize(int n, int m);

    int rowCnt();
    int colCnt();
private:
    std::vector<std::vector<bool>> matrix;
    bool isValidIndexes(int i, int j);
};

const double DEFAULT_MASS = 1.;
const GameTime GAME_TIME_EPS = 1E-9;

/// \brief General class for all physic objects
/**
  The class stores the rectangle and mass of a body.
  Mass is not considered as 'pure' kilos. It's used when calculating
  accelerations (2nd Newton law), e.g. gravity:
  - object with zero mass is not influenced
  - object with mass = m1 will fall slower, than object of mass = m2, if m1 < m2
    (not a classical physic-style)
  - object with infinite mass (static objects) are also not influenced
 */
class PhysicObject
{
public:
    PhysicObject(Rectangle rect, double mass);
    PhysicObject();
    PhysicObject(const PhysicObject &obj);

    Coordinates getCoordinates() const;
    void setCoordinates(Coordinates c);

    Rectangle getRectangle() const;
    double getMass() const;

protected:
    Rectangle rect;
    double mass;

};

/// \brief Class for static objects
/**
  Static object is the object of infinite mass; it can't move
  and gets no effect under any dynamic object's action. Gravity has also
  no influence.
 */
class StaticObject : public PhysicObject
{
public:
    StaticObject(Rectangle rect);
    StaticObject();
    StaticObject(const StaticObject &obj);
};

/// \brief Storage of static objects
/**
  The class represents a tiled obstacle map - set of obstacles which can be
  described by a matrix A (i,j-th element stands for a rectangle of a defined size - a tile):
  - A[i][j] = true, if the i,j-th tile is an obstacle
  - A[i][j] = false, otherwise

  All obstacles are static objects.
  Each physical world holds only one tiled map (or none).
 */
class WorldTiledMap
{
public:
    WorldTiledMap();
    WorldTiledMap(Rectangle tile, TiledMap_ObstaclesMatrix matrix);

    std::vector<StaticObject> obstacles;
};


#endif // PHYS_BASIC_OBJECTS_H
