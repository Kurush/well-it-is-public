#ifndef PHYSIC_WORLD_H
#define PHYSIC_WORLD_H

///\authors Kazakov Kirill

#include <climits>
#include <cmath>
#include <cstdlib>
#include <queue>
#include <random>
#include <unordered_map>
#include <vector>

#include "phys_dynamic_objects.h"
#include "phys_collision_manager.h"

/// PIMPL for PhysicWorld private attributes
struct PWorldContents;

/**
    \brief Class represents the physical world: environment and set of objects

    The class instance is a 2d-world (cartesian coordinates with gravity). May contain a map (set of static objects)
    and physic bodies (set of dynamic objects). Provides methods for adding map and bodies, a collision manager and
    a method for world update (with the parameter of time passed since last update).

    Gravity stands for a force, in order to use mass dependency
*/
class PhysicWorld
{
public:
    PhysicWorld(double gravity);
    PhysicWorld();
    ~PhysicWorld();

    // positive gravity will be accelerating UP!!!
    int getGravity();
    int hasMap();
    int objectsCount();

    void setCollisionManager(InterfaceCollisionManager *mng);

    /**
     \brief adding physical object
     \param obj: pointer to the physical body
     \details Copy is not created, so the world only stores the links.
     \return If successful, object ID in the world is returned (is used to access & remove object)
             Else, negative valur is returned
     */
    int addDynamicObject(DynamicObject *obj);
    void removeDynamicObject(int key);

    void addWorldMap(WorldTiledMap *wMap);
    void removeWorldMap();

    /**
     \brief update the world time
     \param dt: time which the world should "play away"
     \details all the dynamic objects will be updated on dt
     */
    void UpdateTime(GameTime dt);
    /**
     \brief resolve collisions
     \details all the dynamic objects will be updated by:
              - resolving collisions with map
              - resolving collisions with other dynamic objects (several iterations
                are made in order to resolve chain dependencies)
     */
    void UpdateCollisions();
    bool operator == (const PhysicWorld &right) const;

    WorldTiledMap *getMap();

    InterfaceCollisionManager *collisionManager;
private:
    PWorldContents *contents;
};


#endif // PHYSIC_WORLD_H
