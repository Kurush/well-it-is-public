#ifndef PHYSIC_COLLISION_MANAGER_H
#define PHYSIC_COLLISION_MANAGER_H

///\authors Kazakov Kirill

#include <cmath>

#include "phys_dynamic_objects.h"

/// \brief Checker of collisions
/**
  Class provides the group of methods, which define whether two physic objects collide or not,
  and also can calculate the distance between two objects.

  Can be used with no dependence on a physical world, and, as the result,
  no changes can be made to the objects.
 */
class CollisionChecker
{
public:
    bool ifCollide(PhysicObject obj1, PhysicObject obj2);

    /// One-axis collisions. Mind that one-axis collision doesn't guarantee real collision!
    bool ifCollideLeft(PhysicObject obj1, PhysicObject obj2);
    bool ifCollideRight(PhysicObject obj1, PhysicObject obj2);
    bool ifCollideUp(PhysicObject obj1, PhysicObject obj2);
    bool ifCollideDown(PhysicObject obj1, PhysicObject obj2);
    /// return true if object centers are no further than len one from another (carthesian distance: sqrt(dx^2 + dy^2)
    bool ifNear(PhysicObject obj1, PhysicObject obj2, double len);

};

/// Interface for collision manager
class PhysicWorld;
class InterfaceCollisionManager: public CollisionChecker
{
public:
    InterfaceCollisionManager(PhysicWorld *w);
    virtual ~InterfaceCollisionManager() = default;
    virtual int resolveCollisionSD(StaticObject &objS, DynamicObject &objD) = 0;
    virtual int resolveCollisionDD(DynamicObject &obj1, DynamicObject &obj2) = 0;

    virtual int resolveCollisionMap(WorldTiledMap map, DynamicObject &obj) = 0;
    virtual bool ifCollideMap(PhysicObject obj) =0;
protected:
    PhysicWorld *world;
};

/// \brief Manager of collisions
/**
  Class provides set of methods to work with collisions, including:
  - check whether two physic objects collide
  - resolve collisions: static & dynamic, dynamic & dynamic (objects types)
  - resolve collision of a dynamic object with a map

  Each physical world has one (and only one) collision manager,
  so that no collision resolvings can be made for objects from different worlds
 */
class CollisionManager: public InterfaceCollisionManager
{
public:
    CollisionManager(PhysicWorld *w);
    virtual ~CollisionManager() {}

    /// returns 0 if objects didn't collide, 1 otherwise (so do other functions)
    int resolveCollisionSD(StaticObject &objS, DynamicObject &objD);
    int resolveCollisionDD(DynamicObject &obj1, DynamicObject &obj2);

    int resolveCollisionMap(WorldTiledMap map, DynamicObject &obj);
    bool ifCollideMap(PhysicObject obj);
};

#endif // PHYSIC_COLLISION_MANAGER_H
