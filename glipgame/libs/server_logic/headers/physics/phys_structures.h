#ifndef PHYS_STRUCTURES_H
#define PHYS_STRUCTURES_H

///\authors Kazakov Kirill

#include <map>
#include <cstdlib>
#include <vector>

const double STATIC_MASS_INF = 1E18;
const double COORDINATES_EPS = 1e-5;

/// \brief Cartesian coordinates, used through all physic library
struct Coordinates
{
    double x, y;

    Coordinates();
    Coordinates(const Coordinates &c);
    Coordinates(double x, double y);
    bool operator == (const Coordinates &right) const;
};

/// \brief Description of a geometric figure 'rectangle'
/**
  - pos: coordinates of the center of a figure
  - width: semiwidth of a rectangle (X-axis)
  - height: semiheight of a rectangle (Y-axis)
 */
struct Rectangle
{
    Coordinates coords;
    double width;
    double height;

    Rectangle();
    Rectangle(const Rectangle &r);
    Rectangle(double w, double h);
    Rectangle(Coordinates coords, double w, double h);
    Rectangle(double x, double y, double w, double h);
    bool operator == (const Rectangle &right) const;
};

/// \brief Stores the velocity of limited digital values
/**
  - The velocity is divided to vertical (yVel) and horizontal (xVel) components;
  - *VelValue set possible values for each component: xVel must be one of {-xVelValue, 0, xVelValue}
 */
struct DigitalVelocity
{
    double xVel, yVel;
    double xVelValue, yVelValue;

    DigitalVelocity();
    DigitalVelocity(double xVal, double yVal);
    DigitalVelocity(double x, double y, double xVal, double yVal);
    DigitalVelocity(const DigitalVelocity& vel);
    bool operator == (const DigitalVelocity &right) const;

};


#endif // PHYS_STRUCTURES_H
