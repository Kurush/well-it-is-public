#ifndef GAME_LOGGER_H
#define GAME_LOGGER_H

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

///\authors Kazakov Kirill

/// The class receiving all messages (e.g. error messages)
class ServerGameLogger
{
public:
    void report_error(std::string s);
    void print_errors_console();
private:
    std::vector<std::string> errors;
};

#endif // GAME_LOGGER_H
