#ifndef STATIC_LOADER_H
#define STATIC_LOADER_H

#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "physics/phys_basic_objects.h"

struct Sprite;
namespace game_managers {

#define LOAD_STATIC_SUCCESS 0
#define LOAD_STATIC_ERROR 1

enum LoadType
{
  BULLET_LOAD, WEAPON_LOAD,
  PLAYER_LOAD, MAP_LOAD, SPRITE_LOAD
};

class FileManager
{
public:
    FileManager();
    std::string getFilename(LoadType type, int ID);
    bool isSupported(LoadType type);

private:
    std::set<LoadType> supportedLoadTypes;
    std::map<LoadType, std::string> prefixName;
    std::map<LoadType, std::string> suffixName;
};

class SpriteLoader
{
public:
    virtual int loadSprite(int spriteID, int &frameCnt, std::vector<int> &sizes) = 0;
    virtual ~SpriteLoader() = default;
};
class MapLoader
{
public:
    virtual int loadMap(int mapID, std::vector<int> &sizes,
                std::vector<Coordinates> &spawn,
                TiledMap_ObstaclesMatrix &obstacles) = 0;
    virtual ~MapLoader() = default;
};
class BulletLoader
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadBullet(int bulletID, int &spriteID, int &frameCnt,
                   double &velocity, int &damage, std::vector<int> &sizes) = 0;
    virtual ~BulletLoader() = default;
};
class WeaponLoader
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadWeapon(int weaponID, int &spriteID, int &frameCnt,
                   int &bulletID, int &capacity, double &firespeed, std::vector<int> &sizes) = 0;
    virtual ~WeaponLoader() = default;
};
class PlayerLoader
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadPlayer(int playerID, int &spriteID, int &frameCnt,
                   int &HP, double &speedX, double &speedY, std::vector<int> &sizes) = 0;
    virtual ~PlayerLoader() = default;
};

class SpriteLoaderFile: public SpriteLoader, protected virtual FileManager
{
public:
    virtual int loadSprite(int spriteID, int &frameCnt, std::vector<int> &sizes);
};
class MapLoaderFile: public MapLoader, protected virtual FileManager
{
public:
    virtual int loadMap(int mapID, std::vector<int> &sizes,
                std::vector<Coordinates> &spawn,
                TiledMap_ObstaclesMatrix &obstacles);
};
class BulletLoaderFile: public BulletLoader, protected SpriteLoaderFile, protected virtual FileManager
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadBullet(int bulletID, int &spriteID, int &frameCnt,
                   double &velocity, int &damage, std::vector<int> &sizes);
};
class WeaponLoaderFile: public WeaponLoader, protected SpriteLoaderFile, protected virtual FileManager
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadWeapon(int weaponID, int &spriteID, int &frameCnt,
                   int &bulletID, int &capacity, double &firespeed, std::vector<int> &sizes);
};
class PlayerLoaderFile: public PlayerLoader, protected SpriteLoaderFile, protected virtual FileManager
{
public:
    /// sizes - width&height of sprite (for creation of physics)
    virtual int loadPlayer(int playerID, int &spriteID, int &frameCnt,
                   int &HP, double &speedX, double &speedY, std::vector<int> &sizes);
};

}

#endif // STATIC_LOADER_H
