#ifndef GAME_OBJ_STRUCTURES_H
#define GAME_OBJ_STRUCTURES_H

///\authors Kazakov Kirill

#include "package_structures.h"
#include "game_objects/game_logger.h"
#include "game_objects/game_managers.h"
#include "physics/phys_world.h"

const double FRAME_SHIFT_TIME = 0.1 * CLOCKS_PER_SEC;

typedef enum
{
    DIR_LEFT, DIR_RIGHT
} lookDir;


/// \brief Game map
/** When creating, loads map info from file (defined by ID)
 and creates a physic representation of a map. If failed to load from file,
 sets default version of a map.
 */
class GameMap
{
public:
    GameMap(int mapID, ServerGameLogger *logger = nullptr,
            game_managers::MapLoader *loader = new game_managers::MapLoaderFile());
    ~GameMap();

    int getMapID();
    WorldTiledMap* getPhysMap();
    int getWidth();
    int getHeight();
    Coordinates getPlayerSpawnCoord();
    Coordinates getWeaponSpawnCoord();

private:
    int ID;
    Rectangle tile;
    /// heigth & width of map, measured by points (not tiles!)
    int height, width;
    // center coordinates
    std::vector<Coordinates> spawnPoints;
    int cur_spawn;

    WorldTiledMap *physMap;
    game_managers::MapLoader *loader;
};

/// \brief Change coordinate system
/** Physical coordinates are Cartesian, while their game
 representation may be different (e.g. SFML uses "matrix" coordinates: (0,0) is left upper corner).
 Physic coordinates are 'Coordinates', game's - 'Position'. Sp the class offers methods to change the coordinate system.
*/
class CoordinateTranslator
{
public:
    CoordinateTranslator();
    CoordinateTranslator(GameMap *map);
    Coordinates positionToCoordinates(Position pos) const;
    Position coordinatesToPosition(Coordinates coords) const;

    void setMap(GameMap *m);
    GameMap* getMap() const;

private:
    /// game map is needed in order to get map's height
    GameMap *gameMap;
};


/**
should be used every time you need to time-update a sprite
 */
class SpriteUpdater
{
protected:
    /// \param time is player(sprite) time "from the world's creation"
    void updateSpriteTime(Sprite &sprite, GameTime time);
};


#endif // GAME_OBJ_STRUCTURES_H
