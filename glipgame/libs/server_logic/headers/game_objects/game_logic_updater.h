#ifndef GAME_LOGIC_UPDATER_H
#define GAME_LOGIC_UPDATER_H

#include "game_storage.h"

/// number of moving actions in sprite sheet (for calculations of weapon actions)
const int ACTIONS_CNT = 4;
const double PICKUP_NEAR_COEF = 2.5;

enum actionType
{
  LEFT_START, LEFT_END,
  RIGHT_START, RIGHT_END,
  JUMP, SSHOOT,
  PICKWEAPON, THROWWEAPON,

  // these can't be received  from user: inner purpose only
  JUMP_LEFT, JUMP_RIGHT
};

/** map defines which action type defines which action index in sprite sheet
 also, it's considered that having no weapon defines actions 0..3,
 having weapon with ID=0 - actions 4..7, ID = 2: 8..11 etc.
 */
static std::map<actionType, int> actionTypetoID = {{LEFT_START, 1}, {LEFT_END, 1},
                                              {RIGHT_START, 0}, {RIGHT_END, 0},
                                              {JUMP_LEFT, 3}, {JUMP_RIGHT, 2}};

class IDTransformer;
/// \brief updates game objects with inner logic (like shooting a bullet for weapon) & physic bodies (but no collisions management)
/** -  Basic update functions take ID of an object and time, up to which the object should be updated.
    The object itself is loaded from GameStorage, then updated & saved back; basic methods return boolean value,
    which is true if the object after update should be destroyed (bullet hit the wall, player's HP is 0 etc.).
    -  Action update function takes the player id and the action type to process.
 */
class LogicUpdater
{
public:
    LogicUpdater(GameStorage*, PhysicWorld *world, IDTransformer* trans);

    /// returns true, if bullet must destruct
    bool updateBullet(int bulletID, GameTime targetTime);
    bool updateWeapon(int weaponID, GameTime targetTime);
    bool updatePlayer(int playerID, GameTime targetTime);
    /// may change bullet storage -> iterators may be corrupted
    bool updatePlayerAction(int serverPlayerID, actionType action);
private:
    GameStorage *storage;
    PhysicWorld *world;
    /// for defining a storage ID in updatePlayerAction function
    IDTransformer *trans;
};

/// \brief Transfer for server and game IDs
class IDTransformer
{
public:
    IDTransformer();
    void addPlayer(int serverID, int storageID);
    void removePlayer(int serverID);
    /// returns negative is not found
    int getPlayerServer(int storageID);
    /// returns negative is not found
    int getPlayerStorage(int serverID);

private:
    /// 1st is serverID, 2nd - playerID
    std::vector<std::pair<int, int>> IDs;
};

#endif // GAME_LOGIC_UPDATER_H
