#ifndef GAME_STORAGE_H
#define GAME_STORAGE_H

#include <climits>
#include <random>
#include <stdlib.h>
#include <unordered_map>
#include <vector>

#include "game_objects.h"

enum objectType
{
  SBULLET, SWEAPON, SPLAYER
};

class GameStorageIterator;
struct GameStorageData;
/// \brief Storage for dynamic objects
/** Storage for all non-static game objects: bullets, weapons and players.
 Provides functions for adding, deleting, updating and getting (by id) objects (successful adding returns object id).
 Each function requires the type the object ("objectType" enum).
 Also iterator is available (GameStorageIterator)
 */
class GameStorage
{
public:
    GameStorage();
    ~GameStorage();
    /// returns the storage ID of new object
    int add(objectType type, SGameObject *object);
    /// returns 0 if successful, not 1 - otherwise (id not existed)
    int remove(objectType type, int id);
    /// returns 0 if successful, not 1 - otherwise (id not existed)
    int update(objectType type, int id, SGameObject *object);
    /// returns a constant object pointer if element existed, and nullptr otherwise
    const SGameObject* get(objectType type, int id);
private:
    friend class GameStorageIterator;
    GameStorageData *data;

    std::unordered_map<int, SGameObject*> * get_container(objectType type);
};


struct GameStorageIteratorData;
/// \brief Iterator for GameStorage
/** Iterator supports operations with one type of objects.
 To set an iterator (**setIterator** function), storage pointer and object type are required.
 */
class GameStorageIterator
{
public:
    GameStorageIterator();
    ~GameStorageIterator();

    void setIterator(GameStorage *storage, objectType tp);
    /// resets iterator to container's 1st element and returns it
    const SGameObject* set_first();
    /// increment iterator
    void next();
    /// check for reaching of the container's end
    bool isDone();
    /// returns current element (right under iterator)
    const SGameObject* currentVal();
    int currentID();

private:
    GameStorageIteratorData *data;
};

#endif // GAME_STORAGE_H
