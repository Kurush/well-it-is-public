#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <assert.h>
#include <functional>
#include <memory>

class IMessage {
public:
    IMessage(std::string _type) : type(_type) {}
    IMessage() {}
    ~IMessage() {}
    virtual std::string getType() {return type;}
private:
    std::string type;
};


using IEventHandler = std::shared_ptr<IMessage> (std::shared_ptr<IMessage> message);


using IEventHandler = std::shared_ptr<IMessage> (std::shared_ptr<IMessage> message);


class IConnector
{
public:
    /**
            \brief Отправка сообщений в очередь событий
            \param[in] message умный указатель на класс, который реализует интерфейс IMessage

            Для того, чтобы отправлять сообщения, необходимо взять собственный класс-контейнер,
            который будет содержать данные для отправки в другу часть системы. Отнаследовать его от
            класса IMessage и реализовать метод getType. Тип сообщеня должен совпадать с типом события,
            который должен реагировать на это сообщение.
        */
    virtual void sendMessage(std::shared_ptr<IMessage> message) = 0;
    /**
            \brief Регистрация обработчика на определенное событие.
            \param[in] handler указатель на функцию обработчик, для указания
            метода класса воспользуйтесь std::bind

            Регистрация обработчика на определенное событие.  Обработчиков может быть несколько.
            Для регистрации нескольких обработчиков вызовите несколько раз этот метод с разными указателями
            на обработчики, но с одинаковым типом.

        */
    virtual void registerEvent(std::function<IEventHandler> handler, std::string event_type) = 0;
};

class ServerReplacer;
class Connector: public IConnector
{
public:
    Connector();
    Connector(ServerReplacer *);

    void sendMessage(std::shared_ptr<IMessage> message);
    void registerEvent(std::function<IEventHandler> handler, std::string event_type);


private:
    ServerReplacer *server;
    //std::weak_ptr<EventLoop> event_loop;
    //Требуется хранить те события, которые были зарегистрированы через данный коннектор
};


#endif // CONNECTOR_H
