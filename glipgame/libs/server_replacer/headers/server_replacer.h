#ifndef SERVER_REPLACER_H
#define SERVER_REPLACER_H

#include <memory>
#include <functional>
#include <map>
#include <queue>
#include <time.h>
#include <thread>

#include "connector.h"
//#include "server_game.h"

#define SERVER_UPDATE_TIME 0.0001// in seconds

class ServerGame;
class ServerReplacer
{
public:
    ServerReplacer();
    ~ServerReplacer();
    void addMessage(std::shared_ptr<IMessage> message);
    void registerHandler(std::function<IEventHandler> handler, std::string message_type);

    void run();

private:
    ServerGame *serverGame;

    std::thread thrd;

    std::map<std::string, std::function<IEventHandler>> handlers;
    std::queue<std::shared_ptr<IMessage>> messages;
};


#endif // SERVER_REPLACER_H
