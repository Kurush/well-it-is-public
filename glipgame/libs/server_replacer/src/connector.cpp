#include "connector.h"
#include "server_replacer.h"


Connector::Connector(): server(nullptr) {}
Connector::Connector(ServerReplacer *s): server(s) {}

void Connector::sendMessage(std::shared_ptr<IMessage> message)
{
    assert(server != nullptr);
    server->addMessage(message);
}

void Connector::registerEvent(std::function<IEventHandler> handler, std::string event_type)
{
    if (server != nullptr)
    {
        server->registerHandler(handler, event_type);
    }

}


