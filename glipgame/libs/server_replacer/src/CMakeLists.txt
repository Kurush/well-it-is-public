project(server_replacer_src)

include_directories(${HEADERS_ROOT})
include_directories(${SHARED_HEADERS_ROOT})
include_directories(${STATIC_ROOT})
include_directories(${LOOP_DIR})

add_library(server_replacer STATIC
    connector.cpp
    server_replacer.cpp
    )

target_link_libraries(server_replacer
    sub::dataHandler
    sub::data
    sub::userHandler
    sub::drawPart
    sub::dataStorage
    sub::serverHandler
    sub::serverDataHandler
    sub::serverConnector
    sub::types

    sub::mainLoop
    lib_servergame
    )
