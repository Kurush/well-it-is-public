#include "server_replacer.h"
#include "server_game.h"

using namespace std;

#include "Game.h"




void *RunGameLogic(void *connector) {

    auto ptr = make_shared<Connector>(*((Connector *)connector));
    auto *gameLogic = new LogicLoop(ptr);
}


ServerReplacer::ServerReplacer()
{
    Connector connector(this);
    auto ptr = make_shared<Connector>(connector);

    serverGame = new ServerGame(new Connector(this));



    thrd = std::thread(RunGameLogic, &connector);
//    auto *gameLogic = new LogicLoop(ptr);
    std::this_thread::sleep_for(1s);


    int playerID = 0;
    serverGame->addPlayer(playerID);

    // TODO: создать клиента
}
ServerReplacer::~ServerReplacer()
{

    thrd.join();
    delete serverGame;

}

void ServerReplacer::addMessage(std::shared_ptr<IMessage> message)
{
    messages.push(message);
}
void ServerReplacer::registerHandler(std::function<IEventHandler> handler, std::string message_type)
{
    handlers[message_type] = handler;
}

void ServerReplacer::run()
{
    clock_t time = clock();
    double t = 0.;
    bool canParse = false;
    int i = 0 ;
    while (true)
    {
        t += (double)(clock() - time) / CLOCKS_PER_SEC;
        time = clock();

        while (!messages.empty())
        {
            auto m = messages.front();
            string type = messages.front()->getType();

            cout << "$$$ " << type << '\n';
            for (auto h: handlers)
                cout << h.first << '\n';
            cout << "\n###\n";


            if (type == "Init")
            {
                canParse = true;
            }
            if (canParse)
            {
                handlers[type](messages.front());
                ++i;
                std:: cout << i << std:: endl;

            }
            messages.pop();

        }

        //if(i==2)
        //    break;



        if (t > SERVER_UPDATE_TIME)
        {
            serverGame->update(t);
            t = 0;
        }
    }
}
