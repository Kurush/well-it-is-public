#include "game_objects/game_logic_updater.h"
#include "gtest/gtest.h"

using namespace std;

// Game Storage
TEST(testGameStorage, ARG_Bullet)
{
    GameStorage storage;

    PhysicWorld world;
    GameMap m(666);
    SBullet *bullet = new SBullet(666, Position(2,3), &world, &m);

    int id = storage.add(SBULLET, bullet);
    const SBullet* bullet_got = dynamic_cast<const SBullet*>(storage.get(SBULLET, id));
    EXPECT_EQ(*bullet, *bullet_got);

    storage.remove(SBULLET, id);
    bullet_got = dynamic_cast<const SBullet*>(storage.get(SBULLET, id));
    EXPECT_EQ(bullet_got, nullptr);
}
TEST(testGameStorage, UpdateBullet)
{
    GameStorage storage;

    PhysicWorld world;
    GameMap m(666);
    SBullet *bullet = new SBullet(666, Position(2,3), &world, &m);

    int id = storage.add(SBULLET, bullet);

    bullet->setDirection(DIR_LEFT);
    bullet->setPosition(Position(5,18));
    int err = storage.update(SBULLET, id, bullet);
    EXPECT_EQ(err, 0);

    const SBullet* bullet_got = dynamic_cast<const SBullet*>(storage.get(SBULLET, id));
    EXPECT_EQ(*bullet, *bullet_got);

    storage.remove(SBULLET, id);
    bullet_got = dynamic_cast<const SBullet*>(storage.get(SBULLET, id));
    EXPECT_EQ(bullet_got, nullptr);
}
TEST(testGameStorage, ARG_Weapon)
{
    GameStorage storage;

    PhysicWorld world;
    GameMap m(666);
    SWeapon *weapon = new SWeapon(666, Position(2,3), &world, &m);

    int id = storage.add(SWEAPON, weapon);
    const SWeapon* weapon_got = dynamic_cast<const SWeapon*>(storage.get(SWEAPON, id));
    EXPECT_EQ(*weapon, *weapon_got);

    storage.remove(SWEAPON, id);
    weapon_got = dynamic_cast<const SWeapon*>(storage.get(SWEAPON, id));
    EXPECT_EQ(weapon_got, nullptr);
}
TEST(testGameStorage, ARG_Player)
{
    GameStorage storage;

    PhysicWorld world;
    GameMap m(666);
    SPlayer *player = new SPlayer(666, Position(2,3), &world, &m);

    int id = storage.add(SPLAYER, player);
    const SPlayer* player_got = dynamic_cast<const SPlayer*>(storage.get(SPLAYER, id));
    EXPECT_EQ(*player, *player_got);

    storage.remove(SPLAYER, id);
    player_got = dynamic_cast<const SPlayer*>(storage.get(SPLAYER, id));
    EXPECT_EQ(player_got, nullptr);
}

// Game Storage iterator
TEST(testStorageIterator, IterBullets)
{
    PhysicWorld world; // order is important: storage destructor must be called earlier than world's
    GameStorage storage;
    GameMap m(666);
    SBullet *b1 = new SBullet(666, Position(7,-100), &world, &m);
    SBullet *b2 = new SBullet(666, Position(2,3), &world, &m);
    SBullet *b3 = new SBullet(666, Position(2,300), &world, &m);

    set<int> ids;
    ids.insert(storage.add(SBULLET, b1));
    ids.insert(storage.add(SBULLET, b2));
    ids.insert(storage.add(SBULLET, b3));

    GameStorageIterator iter;
    iter.setIterator(&storage, SBULLET);

    // get id, delete it from got ones -> check that all iterated ids are unique
    const SBullet* bullet;
    while (!iter.isDone())
    {
        EXPECT_NE(ids.find(iter.currentID()), ids.end());
        ids.erase(ids.find(iter.currentID()));
        bullet = dynamic_cast<const SBullet*>(iter.currentVal());
        iter.next();
    }
}
TEST(testStorageIterator, IterWeapons)
{
    PhysicWorld world;
    GameStorage storage;
    GameMap m(666);
    SWeapon *w1 = new SWeapon(666, Position(7,-100), &world, &m);
    SWeapon *w2 = new SWeapon(666, Position(2,3), &world, &m);
    SWeapon *w3 = new SWeapon(666, Position(2,300), &world, &m);

    set<int> ids;
    ids.insert(storage.add(SWEAPON, w1));
    ids.insert(storage.add(SWEAPON, w2));
    ids.insert(storage.add(SWEAPON, w3));

    GameStorageIterator iter;
    iter.setIterator(&storage, SWEAPON);

    // get id, delete it from got ones -> check that all iterated ids are unique
    const SWeapon* weapon;
    while (!iter.isDone())
    {
        EXPECT_NE(ids.find(iter.currentID()), ids.end());
        ids.erase(ids.find(iter.currentID()));
        weapon = dynamic_cast<const SWeapon*>(iter.currentVal());
        iter.next();
    }
}
TEST(testStorageIterator, IterPlayers)
{
    PhysicWorld world;
    GameStorage storage;
    GameMap m(666);
    SPlayer *player1 = new SPlayer(666, Position(7,-100), &world, &m);
    SPlayer *player2 = new SPlayer(666, Position(2,3), &world, &m);
    SPlayer *player3 = new SPlayer(666, Position(2,300), &world, &m);

    set<int> ids;
    ids.insert(storage.add(SPLAYER, player1));
    ids.insert(storage.add(SPLAYER, player2));
    ids.insert(storage.add(SPLAYER, player3));

    GameStorageIterator iter;
    iter.setIterator(&storage, SPLAYER);

    // get id, delete it from got ones -> check that all iterated ids are unique
    const SPlayer* player_got;
    while (!iter.isDone())
    {
        EXPECT_NE(ids.find(iter.currentID()), ids.end());
        ids.erase(ids.find(iter.currentID()));
        player_got = dynamic_cast<const SPlayer*>(iter.currentVal());
        iter.next();
    }
}

// ID transformer
TEST(testIDTransformer, CommonTest)
{
    IDTransformer trans;
    trans.addPlayer(1, 11);
    trans.addPlayer(2, 22);
    trans.addPlayer(5, 55);

    EXPECT_EQ(trans.getPlayerServer(22), 2);
    EXPECT_EQ(trans.getPlayerStorage(5), 55);
    EXPECT_LT(trans.getPlayerStorage(7), 0);
    trans.removePlayer(2);
    EXPECT_LT(trans.getPlayerStorage(2), 0);
    EXPECT_LT(trans.getPlayerServer(22), 0);
}
