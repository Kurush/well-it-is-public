#include "game_objects/game_objects.h"
#include "gtest/gtest.h"
#include <cmath>

#define EPS 1e-9

using namespace std;

// Position
TEST(testPosition, ConstructorDefault)
{
    Position pos;
    EXPECT_LT(fabs(pos.x - 0), EPS);
    EXPECT_LT(fabs(pos.y - 0), EPS);
}
TEST(testPosition, ConstructorInteger)
{
    Position pos(1,2);
    EXPECT_LT(fabs(pos.x - 1), EPS);
    EXPECT_LT(fabs(pos.y - 2), EPS);
}
TEST(testPosition, ConstructorDouble)
{
    Position pos(-16.9, 200.173);
    EXPECT_LT(fabs(pos.x + 16.9), EPS);
    EXPECT_LT(fabs(pos.y - 200.173), EPS);
}
TEST(testPosition, ConstructorCopy)
{
    Position pos1(0.1, 2.4);
    Position pos2(pos1);
    EXPECT_LT(fabs(pos2.x - pos1.x), EPS);
    EXPECT_LT(fabs(pos2.y - pos1.y), EPS);
}

// Sprite
TEST(testSprite, ConstructorDefault)
{
    Sprite s;
    EXPECT_EQ(s.ID, 0);
    EXPECT_EQ(s.frame_cnt, 0);
    EXPECT_EQ(s.frame, 0);
    EXPECT_EQ(s.action, 0);
}
TEST(testSprite, ConstructorIDonly)
{
    Sprite s(5);
    EXPECT_EQ(s.ID, 5);
    EXPECT_EQ(s.frame_cnt, 0);
    EXPECT_EQ(s.frame, 0);
    EXPECT_EQ(s.action, 0);
}
TEST(testSprite, ConstructorAll)
{
    Sprite s(1,2,3,4);
    EXPECT_EQ(s.ID, 1);
    EXPECT_EQ(s.action, 2);
    EXPECT_EQ(s.frame, 3);
    EXPECT_EQ(s.frame_cnt, 4);
}
TEST(testSprite, ConstructorCopy)
{
    Sprite s(1,2,3,4), s1(s);
    EXPECT_EQ(s, s1);
}

// GameMap
TEST(testGameMap, Constructor)
{
    GameMap gmap(666);

    Coordinates spawn = gmap.getPlayerSpawnCoord();

    EXPECT_EQ(gmap.getMapID(), 666);
    EXPECT_EQ(gmap.getHeight(), 128);
    EXPECT_EQ(gmap.getWidth(), 128);
    EXPECT_EQ(spawn, Coordinates(16, 16));
}

// Coordinate translator
TEST(testCoordTranslator, PosToCoord)
{
    GameMap gmap(666);

    CoordinateTranslator translator(&gmap);

    Position pos(5, 77);
    Coordinates coord = translator.positionToCoordinates(pos);
    Coordinates coord_exp(5, 128 - 77);

    EXPECT_EQ(coord, coord_exp);
}
TEST(testCoordTranslator, CoordToPos)
{
    GameMap gmap(666);

    CoordinateTranslator translator(&gmap);

    Coordinates coords(5, 77);
    Position pos = translator.coordinatesToPosition(coords);
    Position pos_exp(5, 128 - 77);

    EXPECT_EQ(pos, pos_exp);
}

// Game object
TEST(testGameObject, Constructor1Param)
{
    PhysicWorld world;
    GameMap m(666);
    SGameObject obj(&world, &m);

    EXPECT_EQ(obj.getPosition(), Position(-1, m.getHeight() - 1));
    EXPECT_EQ(obj.sprite, Sprite());
}
TEST(testGameObject, ConstructorPBody)
{
    PhysicWorld world;
    DynamicObject *pobj = new DynamicObject(Rectangle(1,2,3,4), 1., DigitalVelocity(1,2,3,4));
    GameMap m(666);
    SGameObject obj(pobj, &world, &m);

    EXPECT_EQ(obj.getPosition(), Position(-2, m.getHeight() -6));
    EXPECT_EQ(obj.sprite, Sprite());
}
TEST(testGameObject, ConstructorCopy)
{
    PhysicWorld world;
    GameMap m(666);
    DynamicObject *pobj = new DynamicObject(Rectangle(1,2,3,4), 1., DigitalVelocity(1,2,3,4));
    SGameObject obj(pobj, &world, &m);
    SGameObject obj1(obj);

    EXPECT_EQ(obj, obj1);
}

// SBullet
TEST(testBullet, Constructor)
{
    PhysicWorld world;
    GameMap m(666);
    SBullet b(666, Position(2,3), &world, &m);

    EXPECT_EQ(b.getPosition(), Position(2, 3));
    EXPECT_EQ(b.getDamage(), 666);
}
TEST(testBullet, Update)
{
    PhysicWorld world;
    GameMap m(666);
    SBullet b(666, Position(2,3), &world, &m);

    GameTime dt = FRAME_SHIFT_TIME + 0.1;
    b.updateTime(dt);

    EXPECT_EQ(b.getPosition(), Position(2,3));
    EXPECT_EQ(b.getDamage(), 666);
    EXPECT_EQ(b.sprite.frame, 0); // 0 - it didnt move
    EXPECT_EQ(b.getTime(), dt);

    b.getPhysicBody()->velocity.xVel = b.getPhysicBody()->velocity.xVelValue;
    b.updateTime(dt);
    EXPECT_EQ(b.sprite.frame, 2); // 2 - it had velocity
    EXPECT_EQ(b.getTime(), 2*dt);

}
TEST(testBullet, SetDirection)
{
    PhysicWorld world;
    GameMap m(666);
    SBullet b(666, Position(2,3), &world, &m);

    b.setDirection(DIR_LEFT);
    EXPECT_EQ(b.getVelocity(), DigitalVelocity(-5.2,0,5.2,0));

    b.setDirection(DIR_RIGHT);
    EXPECT_EQ(b.getVelocity(), DigitalVelocity(5.2,0,5.2,0));
}


// SWeapon
TEST(testWeapon, Constructor)
{
    PhysicWorld world;
    GameMap m(666);
    SWeapon w(666, Position(2,3), &world, &m);

    EXPECT_EQ(w.getPosition(), Position(2,3));
    EXPECT_EQ(w.getBulletID(), 666);
    EXPECT_EQ(w.getPiked(), false);
    EXPECT_EQ(w.getFireSpeed(), 1.5);
    EXPECT_EQ(w.getTimeFromShot(), 0);
    EXPECT_EQ(w.getCapacity(), 30);
    EXPECT_EQ(w.getTime(), 0);
}
TEST(testWeapon, Pick_Throw)
{
    PhysicWorld world;
    GameMap m(666);
    SWeapon w(666, Position(2,3), &world, &m);


    DynamicObject *pbody = new DynamicObject(*w.getPhysicBody());
    w.onPickUp();
    EXPECT_EQ(w.getPiked(), true);
    EXPECT_EQ(*w.getPhysicBody(), *pbody);

    pbody->setCoordinates(w.positionToCoordinates(Position(5,5)));
    pbody->velocity.xVel = pbody->velocity.yVel = 0;
    w.onThrow(Position(5,5));
    EXPECT_EQ(w.getPiked(), false);
    EXPECT_EQ(w.getPosition(), Position(5,5));

    delete pbody;
}
TEST(testWeapon, Shoot)
{
    PhysicWorld world;
    GameMap m(666);
    SWeapon w(666, Position(2,3), &world, &m);

    // incorrect call.... isn't correct without shooter's consedering
    SBullet *bullet = w.shoot(DIR_RIGHT, 8, w.getPosition());
    EXPECT_EQ(bullet, nullptr);

    GameTime dt = w.getFireSpeed();
    w.updateTime(dt);

    bullet = w.shoot(DIR_RIGHT, 8, w.getPosition());
    EXPECT_EQ(bullet->getPosition().x, w.getPosition().x + 8);
    EXPECT_EQ(bullet->getVelocity(),  DigitalVelocity(5.2,0,5.2,0));
    delete bullet;
}

// SPlayer
TEST(testPlayer, Constructor)
{
    PhysicWorld world;
    GameMap m(666);
    SPlayer p(666, Position(2,3), &world, &m);

    EXPECT_EQ(p.getPosition(), Position(2,3));
    EXPECT_EQ(p.getHP(), 1000);
    //EXPECT_EQ(p.getLookDir(), ... ); TODO ?!!!!!
    EXPECT_LT(p.getWeaponID(), 0);
    EXPECT_EQ(p.getTime(), 0);
}
TEST(testPlayer, ReceiveDamage)
{
    PhysicWorld world;
    GameMap m(666);
    SPlayer p(666, Position(2,3), &world, &m);

    p.receiveDamage(100);
    EXPECT_EQ(p.getHP(), 900);
}
