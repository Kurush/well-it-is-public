#include "server_game.h"
#include "gtest/gtest.h"
#include <cmath>

using namespace std;


struct ServerGameData
{
    PhysicWorld *physWorld;
    PackageManagers *packageManagers;
    GameTime gameTime;
    GameMap *gameMap;
    GameStorage *gameStorage;
    LogicUpdater *logicUpdater;
    IDTransformer idTransformer;
    ServerGameLogger *logger;

    ~ServerGameData()
    {
        delete gameMap;
        delete logicUpdater;
        delete packageManagers;
        delete gameStorage; // the order of deletes is important! storage must be deleted before PhysicWorld
        delete physWorld;
        delete logger;
    }
};

class ServerGameTester: public ServerGame
{
public:
    ServerGameTester(IConnector *connector): ServerGame(connector)
    {
        // left only one weapon
        queue<pair<objectType, int>> toDelete;
        GameStorageIterator iter;
        bool ifDestruct = false;
        // update Weapons
        iter.setIterator(data->gameStorage, SWEAPON);
        while (!iter.isDone())
        {
            if (ifDestruct)
                toDelete.push(make_pair(SWEAPON, iter.currentID()));
            iter.next();
            ifDestruct = true;
        }

        // erase destructed objects from storage
        pair<objectType, int> p;
        while (!toDelete.empty())
        {
            p = toDelete.front();
            toDelete.pop();
            data->gameStorage->remove(p.first, p.second);
        }
    }
    PackageManagers* getPackageManagers()
    {
        return data->packageManagers;
    }

    /// pairs: serverID, SPlayer*
    map<int, SPlayer*> getPlayers()
    {
        map<int, SPlayer*> answer;

        GameStorageIterator players;
        players.setIterator(data->gameStorage, SPLAYER);
        SPlayer *player;
        while (!players.isDone())
        {
            player = const_cast<SPlayer*>(dynamic_cast<const SPlayer*>(players.currentVal()));
            int serverID = this->data->idTransformer.getPlayerServer(players.currentID());
            answer.insert(make_pair(serverID, player));
            players.next();
        }
        return answer;
    }
    map<int, SWeapon*> getWeapons()
    {
        map<int, SWeapon*> answer;

        GameStorageIterator ws;
        ws.setIterator(data->gameStorage, SWEAPON);
        SWeapon *w;
        int i = 0;
        while (!ws.isDone())
        {
            w = const_cast<SWeapon*>(dynamic_cast<const SWeapon*>(ws.currentVal()));
            answer.insert(make_pair(i++, w));
            ws.next();
        }
        return answer;
    }
    map<int, SBullet*> getBullets()
    {
        map<int, SBullet*> answer;

        GameStorageIterator bs;
        bs.setIterator(data->gameStorage, SBULLET);
        SBullet *b;
        int i = 0;
        while (!bs.isDone())
        {
            b = const_cast<SBullet*>(dynamic_cast<const SBullet*>(bs.currentVal()));
            answer.insert(make_pair(i++, b));
            bs.next();
        }
        return answer;
    }


};

class TestConnector: public IConnector
{
public:
    TestConnector() {}
    void sendMessage(std::shared_ptr<IMessage> message)
    {
        if (message->getType() == "Update")
        {
            auto x = std::reinterpret_pointer_cast<server_packages::ServerUpdatePackage>(message);
            while (!x->data.empty())
            {
                auto p = x->data.begin();
                delete *p;
                x->data.erase(x->data.begin());
            }

        }
    }
    void registerEvent(std::function<IEventHandler> handler, std::string event_type) {}
};

TEST(testServerGame, FallNoAction)
{
    ServerGameTester *game = new ServerGameTester(new TestConnector());
    game->addPlayer(2);

    map<int, SPlayer*> players = game->getPlayers();
    // gravity functioning
    EXPECT_EQ(players[2]->getVelocity().yVel, 0);

    game->update(5);
    EXPECT_EQ(players[2]->getPosition().y, 998);
    delete game;

}
TEST(testServerGame, MoveLeftAction)
{
    ServerGameTester *game = new ServerGameTester(new TestConnector());
    game->addPlayer(2);


    PackageManagers* managers = game->getPackageManagers();
    shared_ptr<client_packages::CLIENT_PACKAGE> pack = make_shared<client_packages::CLIENT_PACKAGE>(client_packages::Commands::LEFTBUTTON, 0.5, 2);
    pack->userId = 2;
    pack->typeOfMessage = client_packages::LEFTBUTTON;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));


    map<int, SPlayer*> players = game->getPlayers();
    double startX = players[2]->getPosition().x;
    GameTime dt = 1;
    game->update(dt);

    players = game->getPlayers();
    EXPECT_EQ(players[2]->lookDirection, DIR_LEFT);
    EXPECT_EQ(players[2]->getVelocity().xVel, -players[2]->getVelocity().xVelValue);
    EXPECT_EQ(players[2]->getPosition().x, startX - dt * players[2]->getVelocity().xVelValue);
    delete game;
}
TEST(testServerGame, JumpAction)
{
    ServerGameTester *game = new ServerGameTester(new TestConnector());
    //game->addPlayer(5);
    game->addPlayer(2);
    //game->addPlayer(8);

    game->update(0.1);

    PackageManagers* managers = game->getPackageManagers();
    shared_ptr<client_packages::CLIENT_PACKAGE> pack = make_shared<client_packages::CLIENT_PACKAGE>(client_packages::LEFTBUTTON, 0.5,2);
    pack->userId = 2;
    pack->typeOfMessage = client_packages::UPBUTTON;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));

    map<int, SPlayer*> players = game->getPlayers();
    EXPECT_EQ(players[2]->getVelocity().yVel, players[2]->getVelocity().yVelValue);
    delete game;
}
// test may fall from time to time - dependency on random weapon spawning
TEST(testServerGame, Shoot)
{
    ServerGameTester *game = new ServerGameTester(new TestConnector());
    game->addPlayer(2);
    // in order to fall
    game->update(5);
    map<int, SWeapon*> weapons = game->getWeapons();
    map<int, SPlayer*> players = game->getPlayers();
    Position dest_pos = weapons[0]->getPosition();
    Position cur_pos = players[2]->getPosition();

    // move to reach weapon
    PackageManagers* managers = game->getPackageManagers();
    shared_ptr<client_packages::CLIENT_PACKAGE> pack = make_shared<client_packages::CLIENT_PACKAGE>(client_packages::PICKUP, 0.5, 2);
    pack->userId = 2;
    if (dest_pos.x < cur_pos.x)
        pack->typeOfMessage = client_packages::LEFTBUTTON;
    else
        pack->typeOfMessage = client_packages::RIGHTBUTTON;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));

    while (fabs(cur_pos.x -  dest_pos.x) > players[2]->getPhysicBody()->getRectangle().width * PICKUP_NEAR_COEF)
    {
        game->update(TIME_UPDATE_STEP);
        weapons = game->getWeapons();
        players = game->getPlayers();
        dest_pos = weapons[0]->getPosition();
        cur_pos = players[2]->getPosition();
    }
    // pick up
    pack->typeOfMessage = client_packages::PICKUP;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));

    weapons = game->getWeapons();
    players = game->getPlayers();

    // picking up successful
    EXPECT_NE(players[2]->getWeaponID(), 0);

    // stop the player, rotate him on left
    pack->typeOfMessage = client_packages::LEFTERASE;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));
    EXPECT_EQ(players[2]->getVelocity().xVel, 0);

    // add the target
    game->addPlayer(4);
    // in order to fall
    game->update(5);

    players = game->getPlayers();
    dest_pos = players[4]->getPosition();
    cur_pos = players[2]->getPosition();
    // rotate on target
    if (dest_pos.x < cur_pos.x)
        pack->typeOfMessage = client_packages::LEFTERASE;
    else
        pack->typeOfMessage = client_packages::RIGHTERASE;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));

    pack->typeOfMessage = client_packages::SHOOT;
    managers->inputHandler->eventHandler(std::reinterpret_pointer_cast<IMessage>(pack));

     players = game->getPlayers();
     map<int, SBullet*> bullets = game->getBullets();
     // bullet spawned, on right direction
     EXPECT_EQ(bullets.size(), 1);
     EXPECT_EQ(bullets[0]->getPosition().y,
               players[2]->getPosition().y + players[2]->getPhysicBody()->getRectangle().height/2);
     if (players[2]->lookDirection == DIR_LEFT)
        EXPECT_LT(bullets[0]->getVelocity().xVel, 0);
     else
         EXPECT_GT(bullets[0]->getVelocity().xVel, 0);

     // in order to hit
     int maxHP = players[4]->getHP();
     int damage = bullets[0]->getDamage();
     game->update(500);
     players = game->getPlayers();
     bullets = game->getBullets();
     EXPECT_EQ(bullets.size(), 0);
     EXPECT_EQ(players[4]->getHP(), maxHP - damage);
     delete game;
}
