#include "physics/phys_structures.h"
#include "gtest/gtest.h"

#include <cmath>

#define EPS 1e-9

using namespace std;

// Coordinates
TEST(testCoordinates, ConstructorDefault)
{
    Coordinates coords;
    EXPECT_LT(fabs(coords.x - 0), EPS);
    EXPECT_LT(fabs(coords.y - 0), EPS);
}
TEST(testCoordinates, ConstructorInteger)
{
    Coordinates coords(1,2);
    EXPECT_LT(fabs(coords.x - 1), EPS);
    EXPECT_LT(fabs(coords.y - 2), EPS);
}
TEST(testCoordinates, ConstructorDouble)
{
    Coordinates coords(-16.9, 200.173);
    EXPECT_LT(fabs(coords.x + 16.9), EPS);
    EXPECT_LT(fabs(coords.y - 200.173), EPS);
}
TEST(testCoordinates, ConstructorCopy)
{
    Coordinates coords1(0.1, 2.4);
    Coordinates coords2(coords1);
    EXPECT_LT(fabs(coords2.x - coords1.x), EPS);
    EXPECT_LT(fabs(coords2.y - coords1.y), EPS);
}
TEST(testCoordinates, Comparator)
{
    double x = 5.3, y = -7.99;
    Coordinates c1(x, y);
    Coordinates c2(x, y);
    EXPECT_EQ(c1 == c2, true);

    c1 = Coordinates(0, 0);
    EXPECT_EQ(c1 == c2, false);

    c1 = Coordinates(0, y);
    EXPECT_EQ(c1 == c2, false);

    c1 = Coordinates(x, 0);
    EXPECT_EQ(c1 == c2, false);

    c1 = Coordinates(x - 0.01, y);
    EXPECT_EQ(c1 == c2, false);
}


// Rectangle
TEST(testRectangle, ConstructorDefault)
{
    Rectangle rect;
    Coordinates c(0, 0);
    EXPECT_EQ(rect.coords, c);
    EXPECT_LT(fabs(rect.height - 1.), EPS);
    EXPECT_LT(fabs(rect.width - 1.), EPS);
}
TEST(testRectangle, ConstructorCopy)
{
    Coordinates c(1.4, -7.56);
    Rectangle rect1(c, 1.16, 240);
    Rectangle rect2(rect1);
    EXPECT_EQ(rect1.coords, rect2.coords);
    EXPECT_EQ(rect1.height, rect2.height);
    EXPECT_EQ(rect1.width, rect2.width);
}
TEST(testRectangle, Constructor3params)
{
    Coordinates coords(-1, 2);
    double w = 10.5, h = 4.8;
    Rectangle rect(coords, w, h);
    EXPECT_EQ(rect.coords, coords);
    EXPECT_EQ(rect.height, h);
    EXPECT_EQ(rect.width, w);
}
TEST(testRectangle, Constructor4params)
{
    double x = 5.3, y = -7.99;
    double w = 10.5, h = 4.8;
    Rectangle rect(x, y, w, h);
    EXPECT_EQ(rect.coords.x, x);
    EXPECT_EQ(rect.coords.y, y);
    EXPECT_EQ(rect.height, h);
    EXPECT_EQ(rect.width, w);
}
TEST(testRectangle, Comparator)
{
    double x = 5.3, y = -7.99;
    double w = 10.5, h = 4.8;
    Rectangle rect1(x, y, w, h);
    Rectangle rect2(x, y, w, h);
    EXPECT_EQ(rect1 == rect2, true);

    rect1 = Rectangle(0, 0, 0, 0);
    EXPECT_EQ(rect1 == rect2, false);

    rect1 = Rectangle(x, y, 0, 0);
    EXPECT_EQ(rect1 == rect2, false);

    rect1 = Rectangle(0, 0, w, h);
    EXPECT_EQ(rect1 == rect2, false);

    rect1 = Rectangle(x - 0.01, y, w, h);
    EXPECT_EQ(rect1 == rect2, false);

}

// DigitalVelocity
TEST(testDigitalVelocity, ConstructorDefault)
{
    DigitalVelocity vel;
    EXPECT_EQ(vel.xVel, 0);
    EXPECT_EQ(vel.yVel, 0);
    EXPECT_EQ(vel.xVelValue, 0);
    EXPECT_EQ(vel.yVelValue, 0);
}
TEST(testDigitalVelocity, ConstructorCopy)
{
    DigitalVelocity vel1(1.2, 4.6, 0.4, 18.9);
    DigitalVelocity vel2(vel1);
    EXPECT_LT(fabs(vel1.xVel - vel2.xVel), EPS);
    EXPECT_LT(fabs(vel1.yVel - vel2.yVel), EPS);
    EXPECT_LT(fabs(vel1.xVelValue - vel2.xVelValue), EPS);
    EXPECT_LT(fabs(vel1.yVelValue - vel2.yVelValue), EPS);
}
TEST(testDigitalVelocity, Constructor2params)
{
    double xv = 4.2, yv = 17.982;
    DigitalVelocity vel(xv, yv);
    EXPECT_LT(fabs(vel.xVel - 0.), EPS);
    EXPECT_LT(fabs(vel.yVel - 0.), EPS);
    EXPECT_LT(fabs(vel.xVelValue - xv), EPS);
    EXPECT_LT(fabs(vel.yVelValue - yv), EPS);
}
TEST(testDigitalVelocity, Constructor4params)
{
    double x = 4.2, y = 17.982;
    double xv = 10, yv = 100.23;
    DigitalVelocity vel(x, y, xv, yv);
    EXPECT_LT(fabs(vel.xVel - x), EPS);
    EXPECT_LT(fabs(vel.yVel - y), EPS);
    EXPECT_LT(fabs(vel.xVelValue - xv), EPS);
    EXPECT_LT(fabs(vel.yVelValue - yv), EPS);
}
TEST(testDigitalVelocity, Comparator)
{
    double x = 4.2, y = 17.982;
    double xv = 10, yv = 100.23;

    DigitalVelocity vel1(x, y, xv, yv);
    DigitalVelocity vel2(x, y, xv, yv);
    EXPECT_EQ(vel1 == vel2, true);

    vel1 = DigitalVelocity(0, 0, 0, 0);
    EXPECT_EQ(vel1 == vel2, false);

    vel1 = DigitalVelocity(x, y, 0, 0);
    EXPECT_EQ(vel1 == vel2, false);

    vel1 = DigitalVelocity(0, 0, xv, yv);
    EXPECT_EQ(vel1 == vel2, false);

    vel1 = DigitalVelocity(x - 0.01, y, xv, yv);
    EXPECT_EQ(vel1 == vel2, false);

}



