#include "physics/phys_dynamic_objects.h"
#include "gtest/gtest.h"

#include <cmath>

#define EPS 1e-9

using namespace std;

// PhysicObject
TEST(testPhysicObject, ConstructorDefault)
{
    PhysicObject obj;
    double mass = DEFAULT_MASS;
    Rectangle rect(0, 0, 1, 1);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - mass), EPS);
}
TEST(testPhysicObject, Constructor2params)
{
    double mass = 14.6;
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    PhysicObject obj(rect, mass);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - mass), EPS);
}
TEST(testPhysicObject, ConstructorCopy)
{
    double mass = 14.6;
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    PhysicObject obj1(rect, mass);
    PhysicObject obj2(obj1);
    EXPECT_EQ(obj1.getRectangle(), obj2.getRectangle());
    EXPECT_LT(fabs(obj1.getMass() - obj2.getMass()), EPS);
}
TEST(testPhysicObject, GetterMass)
{
    double mass = 14.6;
    Rectangle rect;
    PhysicObject obj(rect, mass);
    EXPECT_LT(fabs(obj.getMass() -  mass), EPS);
}
TEST(testPhysicObject, GetterRectangle)
{
    double mass = 3;
    Rectangle rect(123.45, 678, 345, 45.5733);
    PhysicObject obj(rect, mass);
    EXPECT_EQ(obj.getRectangle(), rect);
}
TEST(testPhysicObject, GetterCoordinates)
{
    double mass = 3;
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    PhysicObject obj(rect, mass);
    EXPECT_EQ(obj.getCoordinates(), c);
}
TEST(testPhysicObject, SetterCoordinates)
{
    double mass = 3;
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    PhysicObject obj(rect, mass);
    Coordinates c1(-0.1234, 457);
    obj.setCoordinates(c1);
    EXPECT_EQ(obj.getCoordinates(), c1);
}

// StaticObject
TEST(testStaticObject, ConstructorDefault)
{
    StaticObject obj;
    Rectangle rect(0, 0, 1, 1);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - STATIC_MASS_INF), EPS);
}
TEST(testStaticObject, Constructor1param)
{
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    StaticObject obj(rect);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - STATIC_MASS_INF), EPS);
}
TEST(testStaticObject, ConstructorCopy)
{
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    StaticObject obj1(rect);
    StaticObject obj2(obj1);
    EXPECT_EQ(obj1.getRectangle(), obj2.getRectangle());
    EXPECT_LT(fabs(obj1.getMass() -  obj1.getMass()), EPS);
    EXPECT_LT(fabs(obj1.getMass() -  STATIC_MASS_INF), EPS);

}
TEST(testStaticObject, GetterRectangle)
{
    Rectangle rect(123.45, 678, 345, 45.5733);
    StaticObject obj(rect);
    EXPECT_EQ(obj.getRectangle(), rect);
}
TEST(testStaticObject, GetterCoordinates)
{
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    StaticObject obj(rect);
    EXPECT_EQ(obj.getCoordinates(), c);
}
TEST(testStaticObject, SetterCoordinates)
{
    double mass = 3;
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    StaticObject obj(rect);
    Coordinates c1(-0.1234, 457);
    obj.setCoordinates(c1);
    EXPECT_EQ(obj.getCoordinates(), c1);
}



class TestTimeUpdater: public InterfaceTimeUpdater
{
public:
    virtual bool timeUpdate(DynamicObject *obj, GameTime dt, double gravity)
    {
        // react on each test independently

        // test1: dt = 0
        if ((fabs(dt - 0) < EPS) && (fabs(gravity - 1) < EPS) && (fabs(obj->getMass() - 1) < EPS) &&
                (obj->getCoordinates() == Coordinates(1,1)) &&
                (obj->velocity == DigitalVelocity(1,1,1,1)))
        {
            obj->setCoordinates(Coordinates(1,1));
            obj->velocity = DigitalVelocity(1,1,1,1);
            return 0;
        }
        // test2: gravity = 0
        else if ((fabs(dt - 10) < EPS) && (fabs(gravity - 0) < EPS) && (fabs(obj->getMass() - 1) < EPS) &&
                (obj->getCoordinates() == Coordinates(0,0)) &&
                (obj->velocity == DigitalVelocity(1,1,1,1)))
        {
            obj->setCoordinates(Coordinates(10,10));
            obj->velocity = DigitalVelocity(1,1,1,1);
            return 0;
        }
        // test3: mass = 0 (gravity doesn't do anything)
        else if ((fabs(dt - 10) < EPS) && (fabs(gravity - 10) < EPS) && (fabs(obj->getMass() - 0) < EPS) &&
                (obj->getCoordinates() == Coordinates(0,0)) &&
                (obj->velocity == DigitalVelocity(1,1,1,1)))
        {
            obj->setCoordinates(Coordinates(10,10));
            obj->velocity = DigitalVelocity(1,1,1,1);
            return 0;
        }
        // test4: normal
        else if ((fabs(dt - 10) < EPS) && (fabs(gravity + 0.2) < EPS) && (fabs(obj->getMass() - 5) < EPS) &&
                (obj->getCoordinates() == Coordinates(1,2)) &&
                (obj->velocity == DigitalVelocity(-1,9,1,1)))
        {
            obj->setCoordinates(Coordinates(-9,-8));
            obj->velocity = DigitalVelocity(-1,-1,1,1);
            return 0;
        }
        return 1;
    }
};

// DynamicObject
TEST(testDynamicObject, ConstructorDefault)
{
    DynamicObject obj(new TestTimeUpdater);
    double mass = 1;
    DigitalVelocity vel;
    Rectangle rect(0, 0, 1, 1);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - mass), EPS);
    EXPECT_EQ(obj.velocity, vel);
}
TEST(testDynamicObject, Constructor4params)
{
    double mass = 14.6;
    double xv = 16.5, yv = 14.2;
    DigitalVelocity vel(xv, yv);
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    DynamicObject obj(rect, mass, xv, yv);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - mass), EPS);
    EXPECT_EQ(obj.velocity, vel);
}
TEST(testDynamicObject, Constructor3params)
{
    double mass = 14.6;
    double xv = 16.5, yv = 14.2;
    DigitalVelocity vel(xv, yv);
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    DynamicObject obj(rect, mass, vel);
    EXPECT_EQ(obj.getRectangle(), rect);
    EXPECT_LT(fabs(obj.getMass() - mass), EPS);
    EXPECT_EQ(obj.velocity, vel);
}
TEST(testDynamicObject, ConstructorCopy)
{
    double mass = 14.6;
    Rectangle rect(13.6, -1.7, 15.2, 1889.645);
    double xv = 16.5, yv = 14.2, xval = 100, yval = 1e8;
    DigitalVelocity vel(xv, yv, xval, yval);
    DynamicObject obj1(rect, mass, vel);
    DynamicObject obj2(obj1);
    EXPECT_EQ(obj1.getRectangle(), obj2.getRectangle());
    EXPECT_LT(fabs(obj1.getMass() - obj2.getMass()), EPS);
    EXPECT_EQ(obj1.velocity, obj2.velocity);
}
TEST(testDynamicObject, GetterMass)
{
    double mass = 14.6;
    Rectangle rect;
    DigitalVelocity vel;
    DynamicObject obj(rect, mass, vel);
    EXPECT_LT(fabs(obj.getMass() -  mass), EPS);
}
TEST(testDynamicObject, GetterRectangle)
{
    double mass = 3;
    Rectangle rect(123.45, 678, 345, 45.5733);
    DigitalVelocity vel;
    DynamicObject obj(rect, mass, vel);
    EXPECT_EQ(obj.getRectangle(), rect);
}
TEST(testDynamicObject, GetterCoordinates)
{
    double mass = 3;
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel;
    DynamicObject obj(rect, mass, vel);
    EXPECT_EQ(obj.getCoordinates(), c);
}
TEST(testDynamicObject, SetterCoordinates)
{
    double mass = 3;
    Coordinates c(123.63, -2435.54);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel;
    DynamicObject obj(rect, mass, vel);
    Coordinates c1(-0.1234, 457);
    obj.setCoordinates(c1);
    EXPECT_EQ(obj.getCoordinates(), c1);
}

TEST(testDynamicObject, TimeUpdateZeroTime)
{
    double mass = 1;
    Coordinates c(1, 1);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel(1, 1, 1, 1);
    GameTime dt = 0;
    double gravity = 1;

    DynamicObject obj_t(rect, mass, vel, new TestTimeUpdater);
    DynamicObject obj_u(rect, mass, vel);

    obj_t.updateTime(dt, gravity);
    obj_u.updateTime(dt, gravity);
    EXPECT_EQ(obj_t, obj_u);
}
TEST(testDynamicObject, TimeUpdateZeroGravity)
{
    double mass = 1;
    Coordinates c(0, 0);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel(1, 1, 1, 1);
    GameTime dt = 10;
    double gravity = 0;

    DynamicObject obj_t(rect, mass, vel, new TestTimeUpdater);
    DynamicObject obj_u(rect, mass, vel);

    obj_t.updateTime(dt, gravity);
    obj_u.updateTime(dt, gravity);
    EXPECT_EQ(obj_t, obj_u);
}
TEST(testDynamicObject, TimeUpdateZeroMass)
{
    double mass = 0;
    Coordinates c(0, 0);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel(1, 1, 1, 1);
    GameTime dt = 10;
    double gravity = 10;

    DynamicObject obj_t(rect, mass, vel, new TestTimeUpdater);
    DynamicObject obj_u(rect, mass, vel);

    obj_t.updateTime(dt, gravity);
    obj_u.updateTime(dt, gravity);
    EXPECT_EQ(obj_t, obj_u);
}
TEST(testDynamicObject, TimeUpdateNormal)
{
    double mass = 5;
    Coordinates c(1, 2);
    Rectangle rect(c, 1, 1);
    DigitalVelocity vel(-1, 9, 1, 1);
    GameTime dt = 10;
    double gravity = -0.2;

    DynamicObject obj_t(rect, mass, vel, new TestTimeUpdater);
    DynamicObject obj_u(rect, mass, vel);

    obj_t.updateTime(dt, gravity);
    obj_u.updateTime(dt, gravity);
    EXPECT_EQ(obj_t, obj_u);
}


// WorldTiledMap
TEST(testWorldTiledMap, ConstructorDefault)
{
    WorldTiledMap wmap;
    EXPECT_EQ(wmap.obstacles.size(), 0);
}
TEST(testWorldTiledMap, ConstructorNoObst)
{
    Rectangle tilesize(0,0,1,1);

    TiledMap_ObstaclesMatrix matrix;
    matrix.setSize(3, 3);

    WorldTiledMap wmap(tilesize, matrix);

    EXPECT_EQ(wmap.obstacles.size(), 4);
}
TEST(testWorldTiledMap, Constructor2obst)
{
    Rectangle tilesize(0,0,1,1);
    TiledMap_ObstaclesMatrix matrix;
    matrix.setSize(3, 3);
    matrix.set(0,0, true);
    matrix.set(2,2, true);

    WorldTiledMap wmap(tilesize, matrix);

    EXPECT_EQ(wmap.obstacles.size(), 6);
}
TEST(testWorldTiledMap, ConstructorAllObst)
{
    Rectangle tilesize(0,0,1,1);
    TiledMap_ObstaclesMatrix matrix;
    matrix.setSize(4, 8);
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 8; ++j)
            matrix.set(i, j , true);

    WorldTiledMap wmap(tilesize, matrix);

    EXPECT_LE(wmap.obstacles.size(), 8); // algo may be optimized for multiline obstacles
}
TEST(testWorldTiledMap, Constructor4obst)
{
    Rectangle tilesize(0,0,1,1);
    TiledMap_ObstaclesMatrix matrix;
    matrix.setSize(4, 4);
    matrix.set(0,0, true);
    matrix.set(0,1, true);
    matrix.set(0,2, true);

    matrix.set(1,2, true);
    matrix.set(2,2, true);

    matrix.set(0,3, true);
    matrix.set(3,3, true);

    WorldTiledMap wmap(tilesize, matrix);

    EXPECT_EQ(wmap.obstacles.size(), 8);
}
