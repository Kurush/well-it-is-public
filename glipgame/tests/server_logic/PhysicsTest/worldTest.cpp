#include "physics/phys_world.h"
#include "gtest/gtest.h"

#include <cmath>

#define EPS 1e-9

using namespace std;

void print_object(DynamicObject &obj)
{
    printf("DYNOBJECT: xy(%lf %lf), vx(%lf), vy(%lf)\n", obj.getCoordinates().x, obj.getCoordinates().y,
                                                         obj.velocity.xVel, obj.velocity.yVel);
}


// Collision Checker
TEST(testBasicCollisionChecker, Equal)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(3,3,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), true);
}
TEST(testBasicCollisionChecker, HalfLeft)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(4,3,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), true);
}
TEST(testBasicCollisionChecker, HalfDown)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(3,4,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), true);
}
TEST(testBasicCollisionChecker, SideLeft)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(1,3,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), true);
}
TEST(testBasicCollisionChecker, SideDown)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(3,1,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), false);
    EXPECT_EQ(c.ifCollide(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), false);
}
TEST(testBasicCollisionChecker, DiagonalNO)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(5,5,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), false);
    EXPECT_EQ(c.ifCollide(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), false);
}
TEST(testBasicCollisionChecker, DiagonalABIT)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    Rectangle rect2(4.95,4.95,1,1);

    PhysicObject obj1(rect1, mass);
    PhysicObject obj2(rect2, mass);

    CollisionChecker c;

    EXPECT_EQ(c.ifCollide(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideRight(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideLeft(obj1, obj2), true);
    EXPECT_EQ(c.ifCollideUp(obj1, obj2), false);
    EXPECT_EQ(c.ifCollideDown(obj1, obj2), true);
}

class TestCollisionManager: public InterfaceCollisionManager
{
public:
    TestCollisionManager(PhysicWorld *w): InterfaceCollisionManager(w) {}
    int resolveCollisionSD(StaticObject &objS, DynamicObject &objD)
    {
        if (objD.getCoordinates() == Coordinates(3,4))
        {
            objD.setCoordinates(Coordinates(3, 5));
            objD.velocity.yVel = 0;
            return 0;
        }
        if (objD.getCoordinates() == Coordinates(3,3))
        {
            objD.setCoordinates(Coordinates(1, 3));
            objD.velocity.xVel = 0;
            return 0;
        }
        if (objD.getCoordinates() == Coordinates(2,3))
        {
            objD.setCoordinates(Coordinates(1, 3));
            objD.velocity.xVel = 0;
            return 0;
        }
        if (objD.getCoordinates() == Coordinates(4,3))
        {
            objD.setCoordinates(Coordinates(5, 3));
            objD.velocity.xVel = 0;
            return 0;
        }
        if (objD.getCoordinates() == Coordinates(2,2))
        {
            objD.setCoordinates(Coordinates(1, 2));
            objD.velocity.xVel = 0;
            return 0;
        }
        if (objD.getCoordinates() == Coordinates(5,5))
        {
            return 0;
        }

        return 1;
    }
    int resolveCollisionDD(DynamicObject &obj1, DynamicObject &obj2)
    {
        if ((obj1.getCoordinates() == Coordinates(2,5)) && (obj1.velocity.xVel == 2) && (obj2.velocity.xVel == 1))
        {
            obj2.setCoordinates(Coordinates(6, 5));
            return 0;
        }
        else if ((obj1.getCoordinates() == Coordinates(2,5)) && (obj1.velocity.xVel == 0) && (obj2.velocity.xVel == -1))
        {
            obj1.setCoordinates(Coordinates(1, 5));
            return 0;
        }
        else if ((obj1.getCoordinates() == Coordinates(8,5)) && (obj1.velocity.xVel == 0) && (obj2.velocity.xVel == 0))
        {
            obj1.setCoordinates(Coordinates(8.5, 5));
            obj2.setCoordinates(Coordinates(4.5, 5));
            return 0;
        }
        else if ((obj1.getCoordinates() == Coordinates(8,5)) && (obj1.velocity.xVel == 1) && (obj2.velocity.xVel == -1))
        {
            obj1.setCoordinates(Coordinates(8.5, 5));
            obj2.setCoordinates(Coordinates(4.5, 5));
            return 0;
        }
        else if ((obj1.getCoordinates() == Coordinates(5,8)) && (obj1.velocity.yVel == 0) && (obj2.velocity.yVel == -1))
        {
            obj2.setCoordinates(Coordinates(5, 4));
            return 0;
        }
        else if ((obj1.getCoordinates() == Coordinates(5,8)) && (obj1.velocity.yVel == -1) && (obj2.velocity.yVel == 1))
        {
            obj1.setCoordinates(Coordinates(5, 8.5));
            obj2.setCoordinates(Coordinates(5, 4.5));
            obj1.velocity.yVel = 0;
            obj2.velocity.yVel = 0;
            return 0;
        }
        else if ((obj2.getCoordinates() == Coordinates(5,8)) && (obj1.velocity.yVel == -1) && (obj2.velocity.yVel == 0))
        {
            obj1.setCoordinates(Coordinates(5, 4));
            return 0;
        }
        else if ((obj2.getCoordinates() == Coordinates(5,8)) && (obj1.velocity.yVel == 1) && (obj2.velocity.yVel == 0))
        {
            obj2.setCoordinates(Coordinates(5, 9));
            return 0;
        }


        return 1;
    }

    int resolveCollisionMap(WorldTiledMap map, DynamicObject &obj) {return 0;}
    bool ifCollideMap(PhysicObject obj)
    {
        return true;
    }
};

// collssion manager
TEST(testCollisionManager, SDDynUp)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(0,1,1,1);
    Rectangle rect2(3,4,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, SDDynEq)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(1,1,1,1);
    Rectangle rect2(3,3,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, SDDynLeft)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(1,0,1,1);
    Rectangle rect2(2,3,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, SDDynRight)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(1,0,1,1);
    Rectangle rect2(4,3,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);

}
TEST(testCollisionManager, SDNoCollision)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(1,1,1,1);
    Rectangle rect2(5,5,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, SDDiagonal)
{
    double mass = 1;
    Rectangle rect1(3,3,1,1);
    DigitalVelocity vel(1,1,1,1);
    Rectangle rect2(2,2,1,1);
    StaticObject obj1(rect1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionSD(obj1, obj2);
    mng_test->resolveCollisionSD(obj11, obj22);
    EXPECT_EQ(obj2, obj22);
}

TEST(testCollisionManager, DDDynLeft_RR)
{
    double mass = 1;
    Rectangle rect1(2,5,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(2,0,2,2);
    DigitalVelocity vel2(1,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynLeft_SL)
{
    double mass = 1;
    Rectangle rect1(2,5,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(0,0,2,2);
    DigitalVelocity vel2(-1,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynRight_SS)
{
    double mass = 1;
    Rectangle rect1(8,5,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(0,0,2,2);
    DigitalVelocity vel2(0,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynRight_LR)
{
    double mass = 1;
    Rectangle rect1(8,5,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(1,0,2,2);
    DigitalVelocity vel2(-1,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}

TEST(testCollisionManager, DDDynUP_SD)
{
    double mass = 1;
    Rectangle rect1(5,8,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(0,0,1,1);
    DigitalVelocity vel2(0,-1,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynUP_DU)
{
    double mass = 1;
    Rectangle rect1(5,8,2,2);
    Rectangle rect2(5,5,2,2);
    DigitalVelocity vel1(0,-1,1,1);
    DigitalVelocity vel2(0, 1,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynDown_DS)
{
    double mass = 1;
    Rectangle rect1(5,5,2,2);
    Rectangle rect2(5,8,2,2);
    DigitalVelocity vel1(0,-1,1,1);
    DigitalVelocity vel2(0,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testCollisionManager, DDDynDown_US)
{
    double mass = 1;
    Rectangle rect1(5,5,2,2);
    Rectangle rect2(5,8,2,2);
    DigitalVelocity vel1(0,1,1,1);
    DigitalVelocity vel2(0,0,1,1);
    DynamicObject obj1(rect1, mass, vel1), obj11(obj1);
    DynamicObject obj2(rect2, mass, vel2), obj22(obj2);

    PhysicWorld w1(10), w2(10);
    CollisionManager *mng = new CollisionManager(&w1);
    TestCollisionManager *mng_test = new TestCollisionManager(&w2);
    w1.setCollisionManager(mng);
    w2.setCollisionManager(mng_test);

    mng->resolveCollisionDD(obj1, obj2);
    mng_test->resolveCollisionDD(obj11, obj22);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}


// Physic world
TEST(testPhysicWorld, ConstructorDefault)
{
    PhysicWorld w;
    EXPECT_LT(fabs(w.getGravity() - 0), EPS);
    EXPECT_EQ(w.objectsCount(), 0);
    EXPECT_EQ(w.hasMap(), 0);
}
TEST(testPhysicWorld, ConstructorGravity)
{
    PhysicWorld w(10);
    EXPECT_LT(fabs(w.getGravity() - 10), EPS);
    EXPECT_EQ(w.objectsCount(), 0);
    EXPECT_EQ(w.hasMap(), 0);
}
TEST(testPhysicWorld, AddMap)
{
    WorldTiledMap map;
    PhysicWorld w;
    w.addWorldMap(&map);
    EXPECT_EQ(w.hasMap(), 1);
}
TEST(testPhysicWorld, RemoveMap)
{
    WorldTiledMap map;
    PhysicWorld w;
    w.addWorldMap(&map);
    EXPECT_EQ(w.hasMap(), true);
    w.removeWorldMap();
    EXPECT_EQ(w.hasMap(), false);
}
TEST(testPhysicWorld, AddObject)
{
    DynamicObject obj1, obj2;
    PhysicWorld w;
    int key1 = w.addDynamicObject(&obj1);
    int key2 = w.addDynamicObject(&obj2);
    EXPECT_NE(key1, key2);
    EXPECT_EQ(w.objectsCount(), 2);
}
TEST(testPhysicWorld, RemoveObject)
{
    DynamicObject obj1, obj2;
    PhysicWorld w;
    int key1 = w.addDynamicObject(&obj1);
    int key2 = w.addDynamicObject(&obj2);
    EXPECT_NE(key1, key2);
    w.removeDynamicObject(key2);
    EXPECT_EQ(w.objectsCount(), 1);
}
TEST(testPhysicWorld, TimeUpdate)
{
    DynamicObject obj1(Rectangle(Coordinates(0, 0), 1, 1), 1, DigitalVelocity(1, 0, 1, 1));
    DynamicObject obj2(Rectangle(Coordinates(10, 10), 1, 1), 1, DigitalVelocity(-5, 10, 1, 1));

    DynamicObject obj11(Rectangle(Coordinates(1, -10), 1, 1), 1, DigitalVelocity(1, -10, 1, 1));
    DynamicObject obj22(Rectangle(Coordinates(5, 10), 1, 1), 1, DigitalVelocity(-5, 0, 1, 1));

    PhysicWorld w(-10);
    int key1 = w.addDynamicObject(&obj1);
    int key2 = w.addDynamicObject(&obj2);

    w.UpdateTime(1);

    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
}
TEST(testPhysicWorld, CollisionUpdate)
{
    DynamicObject obj1(Rectangle(Coordinates(1, 1), 1, 1), 1, DigitalVelocity(1, 0, 1, 1));
    DynamicObject obj2(Rectangle(Coordinates(2, 1), 1, 1), 1, DigitalVelocity(2, 0, 1, 1));
    DynamicObject obj3(Rectangle(Coordinates(3, 1), 1, 1), 1, DigitalVelocity(0, 0, 1, 1));

    DynamicObject obj11(Rectangle(Coordinates(1, 1), 1, 1), 1, DigitalVelocity(1, 0, 1, 1));
    DynamicObject obj22(Rectangle(Coordinates(3, 1), 1, 1), 1, DigitalVelocity(2, 0, 1, 1));
    DynamicObject obj33(Rectangle(Coordinates(5, 1), 1, 1), 1, DigitalVelocity(0, 0, 1, 1));

    PhysicWorld w(-10);
    int key1 = w.addDynamicObject(&obj1);
    int key2 = w.addDynamicObject(&obj2);
    int key3 = w.addDynamicObject(&obj3);

    CollisionManager *mng = new CollisionManager(&w);
    w.setCollisionManager(mng);

    WorldTiledMap wMap;
    w.addWorldMap(&wMap);

    w.UpdateCollisions();


    EXPECT_EQ(obj1, obj11);
    EXPECT_EQ(obj2, obj22);
    EXPECT_EQ(obj3, obj33);
}
