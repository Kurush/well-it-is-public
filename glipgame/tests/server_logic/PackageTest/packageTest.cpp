#include "packages.h"
#include "gtest/gtest.h"

#include <cmath>

using namespace std;
using namespace server_packages;

TEST(testPackage, TestServerPackagePart)
{
    ServerPackagePart p1(DELETE);
    ServerPackagePart p2(UPDATE);

    EXPECT_EQ(p1.packageType, DELETE);
    EXPECT_EQ(p2.packageType, UPDATE);
}
TEST(testPackage, TestServerPPDelete)
{
    ServerPPDelete d1(BULLET, 5);
    ServerPPDelete d2(WEAPON, 3);
    ServerPPDelete d3(PLAYER, 666);

    EXPECT_EQ(d1.packageType, DELETE);
    EXPECT_EQ(d1.objectType, BULLET);
    EXPECT_EQ(d1.objectID, 5);

    EXPECT_EQ(d2.packageType, DELETE);
    EXPECT_EQ(d2.objectType, WEAPON);
    EXPECT_EQ(d2.objectID, 3);

    EXPECT_EQ(d3.packageType, DELETE);
    EXPECT_EQ(d3.objectType, PLAYER);
    EXPECT_EQ(d3.objectID, 666);
}
TEST(testPackage, TestServerPPUpdate)
{
    ServerPPUpdate u1(BULLET, 5, Sprite(), Position(), 1, 2);
    ServerPPUpdate u2(WEAPON, 3, Sprite(), Position(), 1, 2);
    ServerPPUpdate u3(PLAYER, 666, Sprite(), Position(), 1, 2);

    EXPECT_EQ(u1.packageType, UPDATE);
    EXPECT_EQ(u1.objectType, BULLET);
    EXPECT_EQ(u1.objectID, 5);
    EXPECT_EQ(u1.Vx, 1);
    EXPECT_EQ(u1.Vy, 2);

    EXPECT_EQ(u2.packageType, UPDATE);
    EXPECT_EQ(u2.objectType, WEAPON);
    EXPECT_EQ(u2.objectID, 3);
    EXPECT_EQ(u2.Vx, 1);
    EXPECT_EQ(u2.Vy, 2);

    EXPECT_EQ(u3.packageType, UPDATE);
    EXPECT_EQ(u3.objectType, PLAYER);
    EXPECT_EQ(u3.objectID, 666);
    EXPECT_EQ(u3.Vx, 1);
    EXPECT_EQ(u3.Vy, 2);
}
TEST(testPackage, TestPackageBullet)
{
    ServerPPUpdateBullet b(10, Sprite(), Position(), 1, 2, 14);

    EXPECT_EQ(b.packageType, UPDATE);
    EXPECT_EQ(b.objectType, BULLET);
    EXPECT_EQ(b.objectID, 10);
    EXPECT_EQ(b.damage, 14);
}
TEST(testPackage, TestPackageWeapon)
{
    ServerPPUpdateWeapon w(100, Sprite(), Position(), 1, 2, 15, false);

    EXPECT_EQ(w.packageType, UPDATE);
    EXPECT_EQ(w.objectType, WEAPON);
    EXPECT_EQ(w.objectID, 100);
    EXPECT_EQ(w.capacity, 15);
    EXPECT_EQ(w.isPicked, false);
}
TEST(testPackage, TestPackagePlayer)
{
    ServerPPUpdatePlayer p1(666, Sprite(), Position(), 1, 2, 150);
    ServerPPUpdatePlayer p2(666, Sprite(), Position(), 1, 2, 150, 5);

    EXPECT_EQ(p1.packageType, UPDATE);
    EXPECT_EQ(p1.objectType, PLAYER);
    EXPECT_EQ(p1.objectID, 666);
    EXPECT_EQ(p1.HP, 150);
    EXPECT_LT(p1.weaponID, 0);

    EXPECT_EQ(p2.packageType, UPDATE);
    EXPECT_EQ(p2.objectType, PLAYER);
    EXPECT_EQ(p2.objectID, 666);
    EXPECT_EQ(p2.HP, 150);
    EXPECT_EQ(p2.weaponID, 5);
}

/* TODO
TEST(testPackage, TestServerInitialPackage)
{
    ServerInitialPackage p(100, 15, 17.77);

    EXPECT_EQ(p.playerID, 100);
    EXPECT_EQ(p.mapID, 15);
    EXPECT_LT(fabs(p.time - 17.77), 1e-9);
}
TEST(testPackage, TestServerUpdatelPackage)
{
...
}*/
