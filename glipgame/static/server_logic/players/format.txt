<< FORMAT DESCRIPTION >>>
<sprite id, integer>
<maximal HP, integer>
<X-axis speed, real, positive>
<Y-axis speed, real, positive>  # the bigger, the higher it jumps

<<< EXAMPLE >>>
666
1000
1.2
1.8


<<< player №666 is test one. DO NOT TOUCH IT! >>>

