import sys
from lxml import etree
from lxml.etree import fromstring

def parse_map(filename, spawn_val):
    data = open(filename, "rb").read()
    tree = etree.fromstring(data, parser=etree.XMLParser(encoding='utf-8'))

    # tiles
    map = tree.xpath('/map')
    tilewidth = int(map[0].get('tilewidth'))
    tileheight = int(map[0].get('tileheight'))

    # map size
    ground = tree.xpath('/map/layer[@name="ground"]')
    width = int(ground[0].get('width'))
    height = int(ground[0].get('height'))

    spawn_points = []

    # map bools
    tiles = tree.xpath('/map/layer[@name="ground"]/data/tile')
    matrix = []
    arr = []
    i = 0
    j = 0
    for node in tiles:
        if j == height:
            j = 0
            i += 1
            matrix.append(arr)
            arr = []

        val = int(node.get('gid'))
        arr.append(val)
        j += 1

        if val == spawn_val:
            spawn_points.append([(0.5+j)*width, (0.5+j)*height])


    filename = filename[:-4] + '.map'
    out = open(filename, 'w')
    out.write(str(tilewidth) + ' ' + str(tileheight) + '\n')
    out.write(str(width) + ' ' + str(height) + '\n')
    for line in matrix:
        out.write(' '.join([str(2 if x==spawn_val else 1 if x else 0) for x in line]) + '\n')

    out.close()

if __name__ == "__main__":
    #filename = "map_1.tmx"
    parse_map(sys.argv[1],  int(sys.argv[2]))
