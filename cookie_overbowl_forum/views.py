# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest

from django.template import loader
from django.shortcuts import render
from django.core.paginator import Paginator
from cookie_overbowl.models import *


class Auth(object):
    def __init__(self, auth=0, login="Cookie Monster"):
        self.authorised = auth
        self.login = login

user = Auth(1)

QUESTIONS_PER_PAGE = 20
PAGINATOR_SIZE = 9

def paginate(request, search_type):
    page_num = request.GET.get('page', None)
    try:
        page_num = int(page_num)
    except:
        page_num = 1

    if search_type == "latest":
        paginator = Paginator(list(Question.objects.all()), QUESTIONS_PER_PAGE)
    elif search_type == "hot":
        paginator = Paginator(list(Question.objects.hot()), QUESTIONS_PER_PAGE)
    else:
        paginator = Paginator(list(Question.objects.filter(tags__tag_name=search_type)), QUESTIONS_PER_PAGE)

    try:
        page = paginator.page(page_num)
    except:
        return None, None

    # calculate info for paginator drawing
    draw_page_nums = ['1']
    if page_num >= 5:
        draw_page_nums.append('...')
        draw_page_nums.append(str(page_num - 2))
        draw_page_nums.append(str(page_num - 1))
        draw_page_nums.append(str(page_num))
    else:
        for i in range(2, max(6, page_num + 1)):
            draw_page_nums.append(str(i))
    page_num0 = page_num
    page_num = max(5, page_num)
    if page_num + 5 <= paginator.num_pages:
        draw_page_nums.append(str(page_num + 1))
        draw_page_nums.append(str(page_num + 2))
        draw_page_nums.append('...')
        draw_page_nums.append(str(paginator.num_pages))
    else:
        for i in range(page_num + 1, min(page_num + 5, paginator.num_pages + 1)):
            draw_page_nums.append(str(i))

    return page, {'pages': draw_page_nums,
                     'pag_page': page,
                     'active_page': str(page_num0)}


def login (request):
    t = loader.get_template("login.html")
    context = {'user': user}
    return HttpResponse(t.render(context, request))

def signup (request):
    t = loader.get_template("signup.html")
    context = {'user': user}
    return HttpResponse(t.render(context, request))

def settings (request):
    t = loader.get_template("settings.html")
    context = {'user': user}
    return HttpResponse(t.render(context, request))

def ask (request):
    t = loader.get_template("ask.html")
    context = {'user': user}
    return HttpResponse(t.render(context, request))


def question (request, question_number):
    try:
        question_number = int(question_number)
    except:
        return ValueError

    question = Question.objects.get(pk=question_number)
    answers = Answer.objects.filter(question=question)

    t = loader.get_template("question.html")
    context = {'question': question,
               'answers': answers,
               'user': user,
               }
    return HttpResponse(t.render(context, request))


def index (request):
    questions_page, paginator = paginate(request, "latest")

    t = loader.get_template("index.html")
    context = {'page_type': 'index',
               'questions_page': questions_page,
               'paginator': paginator,
               'user': user}
    return HttpResponse(t.render(context, request))

def hot (request):
    questions_page, paginator = paginate(request, "hot")

    t = loader.get_template("index.html")
    context = {'page_type': 'hot',
               'questions_page': questions_page,
               'paginator': paginator,
               'user': user,}
    return HttpResponse(t.render(context, request))

def search_tag (request, tag):
    questions_page, paginator = paginate(request, tag)

    t = loader.get_template("tag.html")
    context = {'tag_value': tag,
               'page_type': 'tag',
               'questions_page': questions_page,
               'paginator': paginator,
               'user': user, }
    return HttpResponse(t.render(context, request))


def logout (request):
    user.authorised = 0
    return HttpResponseRedirect('/')

def on_login (request):
    user.authorised = 1
    return HttpResponseRedirect('/')
