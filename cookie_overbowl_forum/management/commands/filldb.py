from django.core.management.base import BaseCommand, CommandError
from django.db import models
from django.contrib.auth.models import User
from cookie_overbowl.models import Question, Profile, Tag, Answer, Like_question, Like_answer

from faker import Faker
from random import sample

f = Faker()

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    #def add_arguments(self, parser):
     #   parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        print("Enter number of users to add: ")
        cnt = int(input())
        self.add_users(cnt)

        print("Enter number of tags to add: ")
        cnt = int(input())
        self.add_tags(cnt)

        print("Enter number of questions to add (with answers and likes): ")
        cnt = int(input())
        self.add_questions(cnt)


    def add_users(self, cnt):
        for i in range(cnt):
            user = User(username=f.name())
            user.save()

            # ФОТО!!!!!
            #file = models.File()
            #avatar = f.image_url
            profile = Profile(user=user, nick_name=f.first_name(),
                              email=f.free_email(), rating=f.random_int(-1000, 1000))
            profile.save()

    def add_tags(self, cnt):
        for i in range(cnt):
            tag = Tag(tag_name=f.word())
            tag.save()

    def add_questions(self, cnt):
        profiles = Profile.objects.all()
        tags = Tag.objects.all()

        profiles_cnt = len(profiles)
        tags_cnt = len(tags)
        for i in range(cnt):
            # create question
            profile = profiles[f.random_int(0, profiles_cnt-1)]
            title = f.sentence() + '?'
            content = f.text()
            likes = f.random_int(-10, 10)
            answers_cnt = f.random_int(0, 10)
            question = Question(profile=profile, title=title, content=content, likes=likes, answer_cnt=answers_cnt)
            question.save()

            # add tags
            curtags = sample(list(tags), f.random_int(0, 5))
            for tag in curtags:
                question.tags.add(tag)

            # add likes for question
            dislikes = f.random_int(-10, 10)

            def add_question_like(question, is_dislike):
                prof = profiles[f.random_int(0, profiles_cnt-1)]
                like = Like_question(profile = prof, question=question, is_dislike=is_dislike)
                like.save()
            if question.likes >= 0:
                for j in range(question.likes):
                    add_question_like(question, 0)
                for j in range(dislikes):
                    add_question_like(question, 0)
                    add_question_like(question, 1)
            else:
                for j in range(question.likes):
                    add_like(question, 1)

            # add answers with likes
            for j in range(answers_cnt):
                content = f.text()
                profile = profiles[f.random_int(0, profiles_cnt - 1)]
                likes = f.random_int(-10, 10)
                correct_flag = f.random_int(0,1)
                answer = Answer(content=content, correct_flag=correct_flag, profile=profile, question=question, likes=likes)
                answer.save()

                # add likes
                dislikes = f.random_int(-10, 10)

                def add_answer_like(answer, is_dislike):
                    prof = profiles[f.random_int(0, profiles_cnt - 1)]
                    like = Like_answer(profile=prof, answer=answer, is_dislike=is_dislike)
                    like.save()

                if answer.likes >= 0:
                    for j in range(answer.likes):
                        add_answer_like(answer, 0)
                    for j in range(dislikes):
                        add_answer_like(answer, 0)
                        add_answer_like(answer, 1)
                else:
                    for j in range(answer.likes):
                        add_answer_like(answer, 1)


