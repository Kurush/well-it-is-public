from django.db import models
from django.conf import settings


class Profile(models.Model):
    nick_name = models.CharField(max_length=64)
    email = models.CharField(max_length=256)
    rating = models.IntegerField(default=0)  # amount of likes? a function needed
    avatar = models.ImageField(null=True)

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.nick_name


class QuestionManager(models.Manager):
    def latest(self):
        return self.order_by('-id')


    def hot(self):
        return self.get_queryset().order_by('-likes')

class Question(models.Model):
    title = models.CharField(max_length=128)
    content = models.TextField()
    likes = models.IntegerField(default=0)  # summary of likes and dislikes
    answer_cnt = models.IntegerField(default=0)

    profile = models.ForeignKey("Profile", on_delete=models.CASCADE)
    tags = models.ManyToManyField("Tag")

    objects = QuestionManager()

    def __str__(self):
        return self.title[:20]


class Answer(models.Model):
    content = models.TextField()
    correct_flag = models.BooleanField(default=False)
    likes = models.IntegerField(default=0)  # summary of likes and dislikes

    profile = models.ForeignKey("Profile", on_delete=models.CASCADE)
    question = models.ForeignKey("Question", on_delete=models.CASCADE)

    def __str__(self):
        return "answer <" + self.content[:20] + "> for question <" + str(self.question) + ">"

class Tag(models.Model):
    tag_name = models.CharField(max_length=32)

    def __str__(self):
        return self.tag_name


class Like_question(models.Model):
    profile = models.ForeignKey("Profile", on_delete=models.CASCADE)
    question = models.ForeignKey("Question", on_delete=models.CASCADE)
    is_dislike = models.BooleanField(default=False)

    def __str__(self):
        if (self.is_dislike):
            return "dislike for <" + str(self.question) + "> by <" + str(self.profile) + ">"
        else:
            return "like for <" + str(self.question) + "> by <" + str(self.profile) + ">"

class Like_answer(models.Model):
    profile = models.ForeignKey("Profile", on_delete=models.CASCADE)
    answer = models.ForeignKey("Answer", on_delete=models.CASCADE)
    is_dislike = models.BooleanField(default=False)

    def __str__(self):
        if (self.is_dislike):
            return "dislike for <" + str(self.answer) + "> by <" + str(self.profile) + ">"
        else:
            return "like for <" + str(self.answer) + "> by <" + str(self.profile) + ">"