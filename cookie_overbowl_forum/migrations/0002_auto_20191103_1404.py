# Generated by Django 2.2.5 on 2019-11-03 14:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cookie_overbowl', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='profile',
            new_name='user',
        ),
    ]
